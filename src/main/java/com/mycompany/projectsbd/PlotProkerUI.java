/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JFrame.java to edit this template
 */
package com.mycompany.projectsbd;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import com.github.lgooddatepicker.components.DatePickerSettings;
import java.awt.Image;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Lenovo
 */
public class PlotProkerUI extends javax.swing.JFrame {

    /**
     * Creates new form PlotProkerUI
     */
	int tabRow;
	javax.swing.JTable Table;
    Connection conn;
	Statement stm;
	ResultSet rs;
	ImageIcon rawErrorImage = new ImageIcon("src/main/resources/Error.png");
	ImageIcon rawSuccessImage = new ImageIcon("src/main/resources/Success.png");
	Image errorImage = rawErrorImage.getImage();
	Image successImage = rawSuccessImage.getImage();
	Image errorScaledImage = errorImage.getScaledInstance(50, 50, Image.SCALE_DEFAULT);
	Image successScaledImage = successImage.getScaledInstance(50, 50, Image.SCALE_DEFAULT);
	Icon errorIcon = new ImageIcon( errorScaledImage );
	Icon successIcon = new ImageIcon( successScaledImage );
        //warning icon
        ImageIcon rawWarningImage = new ImageIcon("src/main/resources/Warning.png");
        Image warningImage = rawWarningImage.getImage();
        Image warningScaledImage = warningImage.getScaledInstance(50, 50, Image.SCALE_DEFAULT);
        Icon warningIcon = new ImageIcon( warningScaledImage );
    public PlotProkerUI() {
        initComponents();
		DatePickerSettings dateSettings = new DatePickerSettings();
		dateSettings.setFormatForDatesCommonEra("yyyy-MM-dd");
		dateSettings.setFormatForDatesBeforeCommonEra("uuuu-MM-dd");
		dpPelaksanaan.setSettings(dateSettings);
		String hostname = "localhost";
        String sqlInstanceName = "LAPTOP-M6P0LRI6"; //computer name 
        String sqlDatabase = "dbPerusahaan2022";  //sql server database name
        String sqlUser = "sa";//username akun
        String sqlPassword = "ggilkom"; //passwrod sa account
        String driver = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
        String connectURL = "jdbc:sqlserver://" + hostname + ":1433" 
                        + ";instance=" + sqlInstanceName + ";databaseName=" + sqlDatabase+";encrypt=true;trustServerCertificate=true";
        try{
            Class.forName(driver);
            conn = DriverManager.getConnection(connectURL, sqlUser, sqlPassword);
            System.out.println("Connect to database successful!!"); 
			stm = conn.createStatement();
			rs = stm.executeQuery("SELECT IDProKer,NamaProKer FROM ProgramKerja where deleted=0");
			while(rs.next()){
				cbProker.addItem(rs.getString("IDProKer")+"|"+rs.getString("NamaProKer"));
			}
			rs = stm.executeQuery("SELECT IDDivisi,NamaDivisi FROM Divisi where deleted=0");
			while(rs.next()){
				cbDivisi.addItem(rs.getString("IDDivisi")+"|"+rs.getString("NamaDivisi"));
			}
        }catch(ClassNotFoundException E){
            E.printStackTrace();
        }catch(SQLException E){
            E.printStackTrace();
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        btnInsert = new javax.swing.JButton();
        btnClear = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        cbProker = new javax.swing.JComboBox<>();
        Divisi = new javax.swing.JLabel();
        cbDivisi = new javax.swing.JComboBox<>();
        jLabel4 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        dpPelaksanaan = new com.github.lgooddatepicker.components.DatePicker();
        jPanel2 = new javax.swing.JPanel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        btnBack1 = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Working Program Plan");
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel1.setBackground(new java.awt.Color(57, 67, 76));

        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("WORKING PROGRAM PLAN");
        jLabel1.setFont(new java.awt.Font("Barlow", 1, 24)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));

        btnInsert.setText("Insert");
        btnInsert.setBackground(new java.awt.Color(255, 239, 214));
        btnInsert.setFont(new java.awt.Font("Segoe UI", 1, 16)); // NOI18N
        btnInsert.setForeground(new java.awt.Color(14, 94, 111));
        btnInsert.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnInsertActionPerformed(evt);
            }
        });

        btnClear.setText("Clear");
        btnClear.setBackground(new java.awt.Color(255, 239, 214));
        btnClear.setFont(new java.awt.Font("Segoe UI", 1, 16)); // NOI18N
        btnClear.setForeground(new java.awt.Color(14, 94, 111));
        btnClear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnClearActionPerformed(evt);
            }
        });

        jLabel5.setText("Working Program");
        jLabel5.setFont(new java.awt.Font("SimSun", 1, 18)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(255, 244, 152));

        cbProker.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Choose Working Program" }));
        cbProker.setBackground(new java.awt.Color(255, 255, 255));
        cbProker.setForeground(new java.awt.Color(0, 0, 0));

        Divisi.setText("Division");
        Divisi.setFont(new java.awt.Font("SimSun", 1, 18)); // NOI18N
        Divisi.setForeground(new java.awt.Color(255, 244, 152));

        cbDivisi.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Choose Division" }));
        cbDivisi.setBackground(new java.awt.Color(255, 255, 255));
        cbDivisi.setForeground(new java.awt.Color(0, 0, 0));

        jLabel4.setText("Implementation");
        jLabel4.setFont(new java.awt.Font("SimSun", 1, 18)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(255, 244, 152));

        jLabel7.setText("Date");
        jLabel7.setFont(new java.awt.Font("SimSun", 1, 18)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(255, 244, 152));

        dpPelaksanaan.setBackground(new java.awt.Color(241, 244, 204));
        dpPelaksanaan.setForeground(new java.awt.Color(0, 0, 204));

        jPanel2.setBackground(new java.awt.Color(107, 196, 196));

        jLabel8.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel8.setText("Let's Fill !");
        jLabel8.setFont(new java.awt.Font("Futura XBlkIt BT", 1, 24)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(50, 43, 27));

        jLabel9.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel9.setText("Please fill all of");
        jLabel9.setFont(new java.awt.Font("Futura Md BT", 0, 18)); // NOI18N
        jLabel9.setForeground(new java.awt.Color(50, 43, 27));

        jLabel10.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel10.setText("the section on the form");
        jLabel10.setFont(new java.awt.Font("Futura Md BT", 0, 18)); // NOI18N
        jLabel10.setForeground(new java.awt.Color(50, 43, 27));

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jLabel9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jLabel10, javax.swing.GroupLayout.DEFAULT_SIZE, 233, Short.MAX_VALUE)
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(143, 143, 143)
                .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel9)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel10)
                .addContainerGap(181, Short.MAX_VALUE))
        );

        btnBack1.setText("Back");
        btnBack1.setBackground(new java.awt.Color(255, 239, 214));
        btnBack1.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        btnBack1.setForeground(new java.awt.Color(14, 94, 111));
        btnBack1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBack1ActionPerformed(evt);
            }
        });

        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel2.setText("DATA INPUT");
        jLabel2.setFont(new java.awt.Font("Barlow", 1, 24)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(14, 14, 14)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 360, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(btnInsert, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel4)
                                    .addComponent(jLabel7))
                                .addGap(9, 9, 9))
                            .addComponent(jLabel5))
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(18, 18, 18)
                                .addComponent(dpPelaksanaan, javax.swing.GroupLayout.PREFERRED_SIZE, 199, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(55, 55, 55)
                                .addComponent(btnClear, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(cbProker, javax.swing.GroupLayout.PREFERRED_SIZE, 199, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 360, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnBack1)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(Divisi)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(cbDivisi, javax.swing.GroupLayout.PREFERRED_SIZE, 199, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addComponent(jLabel1)
                .addGap(0, 0, 0)
                .addComponent(jLabel2)
                .addGap(39, 39, 39)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(Divisi)
                    .addComponent(cbDivisi, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(cbProker, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(dpPelaksanaan, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel4)
                        .addGap(2, 2, 2)
                        .addComponent(jLabel7)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnInsert, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnClear, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(42, 42, 42)
                .addComponent(btnBack1, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(25, 25, 25))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        getContentPane().add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 620, 420));

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

	public void passTable(javax.swing.JTable Table,int i){
		this.Table = Table;
		this.tabRow = i;
	}
    private void btnInsertActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnInsertActionPerformed
        // TODO add your handling code here:
        if (cbDivisi.getSelectedIndex() == 0 || cbProker.getSelectedIndex() == 0 || dpPelaksanaan.getText().equals("")) {
			JOptionPane.showMessageDialog(this, "Please fill the blank sections!", "Warning", JOptionPane.ERROR_MESSAGE, warningIcon);
		} else {
			try {
				if (btnInsert.getText().equals("Insert")) {
					rs = stm.executeQuery("SELECT * FROM Rencana WHERE IDDivisi='" + cbDivisi.getSelectedItem().toString().substring(0, 6)
							+ "' and IDProKer='" + cbProker.getSelectedItem().toString().substring(0, 6) + "'");
					if (rs.next()) {
						JOptionPane.showMessageDialog(this, "Record already exists!", "Error", JOptionPane.ERROR_MESSAGE, errorIcon);
					} else {
						stm.executeUpdate("INSERT INTO Rencana VALUES ('" + cbDivisi.getSelectedItem().toString().substring(0, 6) + "','"
								+ cbProker.getSelectedItem().toString().substring(0, 6) + "','" + dpPelaksanaan.getText() + "')");
						JOptionPane.showMessageDialog(this, "Insert successful!", "Success", JOptionPane.INFORMATION_MESSAGE, successIcon);
					}
				} else {
					stm.executeUpdate("UPDATE Rencana SET IDDivisi='" + cbDivisi.getSelectedItem().toString().substring(0, 6)+ "',IDProKer='" + cbProker.getSelectedItem().toString().substring(0, 6) 
							+ "',TglPelaksanaan='"+dpPelaksanaan.getText()+"' FROM ProgramKerja,Divisi WHERE Rencana.IDDivisi=Divisi.IDDivisi and Rencana.IDProKer=ProgramKerja.IDProKer "
							+ "and NamaProker='"+Table.getValueAt(tabRow,0)+"' and NamaDivisi='"+Table.getValueAt(tabRow, 1)+"'");
					rs = stm.executeQuery("SELECT COUNT(*) FROM Rencana " +
									"LEFT JOIN ProgramKerja PK ON PK.IDProKer=Rencana.IDProKer and PK.deleted=0 " +
									"LEFT JOIN Divisi ON Divisi.IDDivisi=Rencana.IDDivisi and Divisi.deleted=0 " +
									"WHERE Divisi.deleted=0 or PK.deleted=0");
					rs.next();
					int row = rs.getInt(1);
					int col = 3;
					String[] colName = {"Divisi", "Program Kerja", "Tanggal Pelaksanaan"};
					String[][] data = new String[row][col];
					rs = stm.executeQuery("SELECT NamaDivisi, NamaProker, TglPelaksanaan FROM Rencana " +
										"LEFT JOIN ProgramKerja PK ON PK.IDProKer=Rencana.IDProKer and PK.deleted=0 " +
										"LEFT JOIN Divisi ON Divisi.IDDivisi=Rencana.IDDivisi and Divisi.deleted=0 " +
										"WHERE Divisi.deleted=0 or PK.deleted=0");
					int i = 0;
					while (rs.next()) {
						for (int j = 0; j < col; j++) {
							data[i][j] = rs.getString(j + 1);
						}
						i++;
					}
					DefaultTableModel model = (DefaultTableModel) Table.getModel();
					model.setDataVector(data, colName);
					dispose();
				}
			} catch (SQLException E) {
				E.printStackTrace();
			}
		}
		
    }//GEN-LAST:event_btnInsertActionPerformed

    private void btnClearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnClearActionPerformed
        // TODO add your handling code here:
		cbProker.setSelectedIndex(0);
		cbDivisi.setSelectedIndex(0);
		dpPelaksanaan.setText("");
    }//GEN-LAST:event_btnClearActionPerformed

    private void btnBack1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBack1ActionPerformed
        // TODO add your handling code here:
        if(btnInsert.getText().equals("Insert")){
			MainUI main = new MainUI();
			main.setVisible(true);
			main.setLabel("Choose Data to Input");
			dispose();
		}else{
			dispose();
		}
    }//GEN-LAST:event_btnBack1ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(PlotProkerUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(PlotProkerUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(PlotProkerUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(PlotProkerUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new PlotProkerUI().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel Divisi;
    private javax.swing.JButton btnBack1;
    private javax.swing.JButton btnClear;
    public javax.swing.JButton btnInsert;
    public javax.swing.JComboBox<String> cbDivisi;
    public javax.swing.JComboBox<String> cbProker;
    public com.github.lgooddatepicker.components.DatePicker dpPelaksanaan;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    // End of variables declaration//GEN-END:variables
}
