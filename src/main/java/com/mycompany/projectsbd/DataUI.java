/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JFrame.java to edit this template
 */
package com.mycompany.projectsbd;

import com.github.lgooddatepicker.components.DatePickerSettings;
import java.awt.Color;
import java.awt.Image;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

/**
 *
 * @author Lenovo
 */
public class DataUI extends javax.swing.JFrame {

	Connection conn;
	Statement stm;
	ResultSet rs;
	String label;
	ImageIcon rawWarningImage = new ImageIcon("src/main/resources/Warning.png");
	Image warningImage = rawWarningImage.getImage();
	Image warningScaledImage = warningImage.getScaledInstance(50, 50, Image.SCALE_DEFAULT);
	Icon warningIcon = new ImageIcon(warningScaledImage);

	public void setLabel(String lbl) {
		this.label = lbl;
		lblJudul.setText(lbl);
		try {
			if ("Division Data".equals(label)) {
				rs = stm.executeQuery("SELECT COUNT(*) FROM Divisi WHERE deleted=0");
				rs.next();
				int row = rs.getInt(1);
				int col = 3;
				String[] colName = {"ID Divisi", "Nama Divisi", "Bidang"};
				String[][] data = new String[row][col];
				rs = stm.executeQuery("SELECT IDDivisi, NamaDivisi, Bidang FROM Divisi WHERE deleted=0");
				int i = 0;
				while (rs.next()) {
					for (int j = 0; j < col; j++) {
						data[i][j] = rs.getString(j + 1);
					}
					i++;
				}
				ShowData(colName, data);
				for (int j = 0; j < colName.length; j++) {
					cbAttribute.addItem(colName[j]);
				}
			}
			if ("Employee Data".equals(label)) {
				rs = stm.executeQuery("SELECT COUNT(*) FROM Karyawan " +
									"LEFT JOIN Divisi ON Divisi.IDDivisi = Karyawan.IDMenempati and Divisi.deleted=0 " +
									"LEFT JOIN Keahlian ON Keahlian.IDKeahlian = Karyawan.IDKeahlian and Keahlian.deleted=0 " +
									"WHERE Karyawan.deleted=0");
				rs.next();
				int row = rs.getInt(1);
				int col = 6;
				String[] colName = {"ID Karyawan", "Nama", "Alamat", "Tanggal Lahir", "Divisi", "Keahlian"};
				String[][] data = new String[row][col];
				rs = stm.executeQuery("SELECT IDKaryawan, Nama, Alamat, TglLahir, NamaDivisi, NamaKeahlian FROM Karyawan " +
									"LEFT JOIN Divisi ON Divisi.IDDivisi = Karyawan.IDMenempati and Divisi.deleted=0 " +
									"LEFT JOIN Keahlian ON Keahlian.IDKeahlian = Karyawan.IDKeahlian and Keahlian.deleted=0 " +
									"WHERE Karyawan.deleted=0");
				int i = 0;
				while (rs.next()) {
					for (int j = 0; j < col; j++) {
						data[i][j] = rs.getString(j + 1);
					}
					i++;
				}
				ShowData(colName, data);
				for (int j = 0; j < colName.length; j++) {
					cbAttribute.addItem(colName[j]);
				}
			}
			if ("Expertise Data".equals(label)) {
				rs = stm.executeQuery("SELECT COUNT(*) FROM Keahlian WHERE deleted=0");
				rs.next();
				int row = rs.getInt(1);
				int col = 3;
				String[] colName = {"ID Keahlian", "Nama Keahlian", "Keterangan"};
				String[][] data = new String[row][col];
				rs = stm.executeQuery("SELECT IDKeahlian, NamaKeahlian, Keterangan FROM Keahlian WHERE deleted=0");
				int i = 0;
				while (rs.next()) {
					for (int j = 0; j < col; j++) {
						data[i][j] = rs.getString(j + 1);
					}
					i++;
				}
				ShowData(colName, data);
				for (int j = 0; j < colName.length; j++) {
					cbAttribute.addItem(colName[j]);
				}
			}
			if ("Position Data".equals(label)) {
				rs = stm.executeQuery("SELECT COUNT(*) FROM Pangkat WHERE deleted=0");
				rs.next();
				int row = rs.getInt(1);
				int col = 3;
				String[] colName = {"ID Pangkat", "Nama Kepangkatan", "Keterangan"};
				String[][] data = new String[row][col];
				rs = stm.executeQuery("SELECT IDPangkat, NamaKepangkatan, Keterangan FROM Pangkat WHERE deleted=0");
				int i = 0;
				while (rs.next()) {
					for (int j = 0; j < col; j++) {
						data[i][j] = rs.getString(j + 1);
					}
					i++;
				}
				ShowData(colName, data);
				for (int j = 0; j < colName.length; j++) {
					cbAttribute.addItem(colName[j]);
				}
			}
			if ("Working Program Data".equals(label)) {
				rs = stm.executeQuery("SELECT COUNT(*) FROM ProgramKerja WHERE deleted=0");
				rs.next();
				int row = rs.getInt(1);
				int col = 3;
				String[] colName = {"ID ProKer", "Nama ProKer", "Keterangan"};
				String[][] data = new String[row][col];
				rs = stm.executeQuery("SELECT IDProKer, NamaProker, Keterangan FROM ProgramKerja WHERE deleted=0");
				int i = 0;
				while (rs.next()) {
					for (int j = 0; j < col; j++) {
						data[i][j] = rs.getString(j + 1);
					}
					i++;
				}
				ShowData(colName, data);
				for (int j = 0; j < colName.length; j++) {
					cbAttribute.addItem(colName[j]);
				}
			}
			if ("Working Program Plan Data".equals(label)) {
				btnDelete.setVisible(false);
				rs = stm.executeQuery("SELECT COUNT(*) FROM Rencana " +
									"LEFT JOIN ProgramKerja PK ON PK.IDProKer=Rencana.IDProKer and PK.deleted=0 " +
									"LEFT JOIN Divisi ON Divisi.IDDivisi=Rencana.IDDivisi and Divisi.deleted=0 " +
									"WHERE Divisi.deleted=0 or PK.deleted=0");
				rs.next();
				int row = rs.getInt(1);
				int col = 3;
				String[] colName = {"Divisi", "Program Kerja", "Tanggal Pelaksanaan"};
				String[][] data = new String[row][col];
				rs = stm.executeQuery("SELECT NamaDivisi, NamaProker, TglPelaksanaan FROM Rencana " +
									"LEFT JOIN ProgramKerja PK ON PK.IDProKer=Rencana.IDProKer and PK.deleted=0 " +
									"LEFT JOIN Divisi ON Divisi.IDDivisi=Rencana.IDDivisi and Divisi.deleted=0 " +
									"WHERE Divisi.deleted=0 or PK.deleted=0");
				int i = 0;
				while (rs.next()) {
					for (int j = 0; j < col; j++) {
						data[i][j] = rs.getString(j + 1);
					}
					i++;
				}
				ShowData(colName, data);
				for (int j = 0; j < colName.length; j++) {
					cbAttribute.addItem(colName[j]);
				}
			}
			if ("Supervisor Data".equals(label)) {
				btnDelete.setVisible(false);
				btnUpdate.setVisible(false);
				rs = stm.executeQuery("SELECT COUNT(*) FROM Karyawan S,Karyawan A " +
									"WHERE S.IDKaryawan=A.IDSupervisor and A.deleted=0 and S.deleted=0");
				rs.next();
				int row = rs.getInt(1);
				int col = 2;
				String[] colName = {"Nama Supervisor", "Nama Karyawan"};
				String[][] data = new String[row][col];
				rs = stm.executeQuery("SELECT S.Nama, A.Nama FROM Karyawan S,Karyawan A " +
									"WHERE S.IDKaryawan=A.IDSupervisor and A.deleted=0 and S.deleted=0");
				int i = 0;
				while (rs.next()) {
					for (int j = 0; j < col; j++) {
						data[i][j] = rs.getString(j + 1);
					}
					i++;
				}
				ShowData(colName, data);
				for (int j = 0; j < colName.length; j++) {
					cbAttribute.addItem(colName[j]);
				}
			}
			if ("Division Superintendent Data".equals(label)) {
				btnDelete.setVisible(false);
				btnUpdate.setVisible(false);
				rs = stm.executeQuery("SELECT COUNT(*) FROM Divisi " +
									"LEFT JOIN Karyawan ON Divisi.IDDivisi=Karyawan.IDMengawas and Karyawan.deleted=0 " +
									"WHERE Divisi.deleted=0");
				rs.next();
				int row = rs.getInt(1);
				int col = 2;
				String[] colName = {"Divisi", "Pengawas Divisi"};
				String[][] data = new String[row][col];
				rs = stm.executeQuery("SELECT NamaDivisi, Nama FROM Divisi " +
									"LEFT JOIN Karyawan ON Divisi.IDDivisi=Karyawan.IDMengawas and Karyawan.deleted=0 " +
									"WHERE Divisi.deleted=0");
				int i = 0;
				while (rs.next()) {
					for (int j = 0; j < col; j++) {
						data[i][j] = rs.getString(j + 1);
					}
					i++;
				}
				ShowData(colName, data);
				for (int j = 0; j < colName.length; j++) {
					cbAttribute.addItem(colName[j]);
				}
			}
			if ("Head of Division Data".equals(label)) {
				btnDelete.setVisible(false);
				rs = stm.executeQuery("SELECT COUNT(*) FROM Divisi " +
									"LEFT JOIN Karyawan ON Divisi.IDKepala=Karyawan.IDKaryawan and Karyawan.deleted=0 " +
									"WHERE Divisi.deleted=0");
				rs.next();
				int row = rs.getInt(1);
				int col = 2;
				String[] colName = {"Divisi", "Kepala Divisi"};
				String[][] data = new String[row][col];
				rs = stm.executeQuery("SELECT NamaDivisi, Nama FROM Divisi " +
									"LEFT JOIN Karyawan ON Divisi.IDKepala=Karyawan.IDKaryawan and Karyawan.deleted=0 " +
									"WHERE Divisi.deleted=0");
				int i = 0;
				while (rs.next()) {
					for (int j = 0; j < col; j++) {
						data[i][j] = rs.getString(j + 1);
					}
					i++;
				}
				ShowData(colName, data);
				for (int j = 0; j < colName.length; j++) {
					cbAttribute.addItem(colName[j]);
				}
			}
			if ("Employee's Position Data".equals(label)) {
				btnDelete.setVisible(false);
				rs = stm.executeQuery("SELECT COUNT(*) FROM Berpangkat,Pangkat,Karyawan " +
									"WHERE Pangkat.IDPangkat=Berpangkat.IDPangkat and Karyawan.IDKaryawan=Berpangkat.IDKaryawan " +
									"and Karyawan.deleted=0 and Pangkat.deleted=0");
				rs.next();
				int row = rs.getInt(1);
				int col = 3;
				String[] colName = {"Nama Karyawan", "Pangkat", "Tanggal Menjabat"};
				String[][] data = new String[row][col];
				rs = stm.executeQuery("SELECT Nama, NamaKepangkatan, TglMenjabat FROM Berpangkat,Pangkat,Karyawan " +
									"WHERE Pangkat.IDPangkat=Berpangkat.IDPangkat and Karyawan.IDKaryawan=Berpangkat.IDKaryawan " +
									"and Karyawan.deleted=0 and Pangkat.deleted=0");
				int i = 0;
				while (rs.next()) {
					for (int j = 0; j < col; j++) {
						data[i][j] = rs.getString(j + 1);
					}
					i++;
				}
				ShowData(colName, data);
				for (int j = 0; j < colName.length; j++) {
					cbAttribute.addItem(colName[j]);
				}
			}
		} catch (SQLException E) {
			E.printStackTrace();
		}
	}

	public DataUI() {
		initComponents();
		String hostname = "localhost";
		String sqlInstanceName = "LAPTOP-M6P0LRI6"; //computer name 
		String sqlDatabase = "dbPerusahaan2022";  //sql server database name
		String sqlUser = "sa";//username akun
		String sqlPassword = "ggilkom"; //passwrod sa account
		String driver = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
		String connectURL = "jdbc:sqlserver://" + hostname + ":1433"
				+ ";instance=" + sqlInstanceName + ";databaseName=" + sqlDatabase + ";encrypt=true;trustServerCertificate=true";
		try {
			Class.forName(driver);
			conn = DriverManager.getConnection(connectURL, sqlUser, sqlPassword);
			System.out.println("Connect to database successful!!");
			stm = conn.createStatement();
		} catch (ClassNotFoundException E) {
			E.printStackTrace();
		} catch (SQLException E) {
			E.printStackTrace();
		}
	}

	/**
	 * This method is called from within the constructor to initialize the form. WARNING: Do NOT modify this code. The content of this method is always regenerated by the Form Editor.
	 */
	@SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lblJudul = new javax.swing.JLabel();
        btnUpdate = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        Table = new javax.swing.JTable();
        txtSearch = new com.mycompany.projectsbd.MyTextField();
        cbAttribute = new javax.swing.JComboBox<>();
        btnSearch = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        btnDelete = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Data");
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lblJudul.setFont(new java.awt.Font("Barlow", 1, 18)); // NOI18N
        lblJudul.setForeground(new java.awt.Color(255, 255, 255));
        lblJudul.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblJudul.setText("Data");
        getContentPane().add(lblJudul, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 12, 620, 30));

        btnUpdate.setBackground(new java.awt.Color(183, 209, 250));
        btnUpdate.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        btnUpdate.setForeground(new java.awt.Color(20, 42, 190));
        btnUpdate.setText("Update");
        btnUpdate.setBorderPainted(false);
        btnUpdate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUpdateActionPerformed(evt);
            }
        });
        getContentPane().add(btnUpdate, new org.netbeans.lib.awtextra.AbsoluteConstraints(490, 380, -1, -1));

        Table.setBackground(new java.awt.Color(0, 153, 153));
        Table.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane2.setViewportView(Table);

        getContentPane().add(jScrollPane2, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 94, 620, 270));

        txtSearch.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtSearchFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtSearchFocusLost(evt);
            }
        });
        txtSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtSearchActionPerformed(evt);
            }
        });
        getContentPane().add(txtSearch, new org.netbeans.lib.awtextra.AbsoluteConstraints(414, 60, 122, -1));

        cbAttribute.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Choose Attribute" }));
        getContentPane().add(cbAttribute, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 60, -1, 28));

        btnSearch.setBackground(new java.awt.Color(183, 209, 250));
        btnSearch.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        btnSearch.setForeground(new java.awt.Color(20, 42, 190));
        btnSearch.setLabel("Search");
        btnSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSearchActionPerformed(evt);
            }
        });
        getContentPane().add(btnSearch, new org.netbeans.lib.awtextra.AbsoluteConstraints(540, 60, -1, -1));

        jButton2.setBackground(new java.awt.Color(183, 209, 250));
        jButton2.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        jButton2.setForeground(new java.awt.Color(20, 42, 190));
        jButton2.setText("Back");
        jButton2.setBorderPainted(false);
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        getContentPane().add(jButton2, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 380, -1, -1));

        btnDelete.setBackground(new java.awt.Color(183, 209, 250));
        btnDelete.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        btnDelete.setForeground(new java.awt.Color(20, 42, 190));
        btnDelete.setText("Delete");
        btnDelete.setBorderPainted(false);
        btnDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });
        getContentPane().add(btnDelete, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 380, -1, -1));

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Gambar2.jpeg"))); // NOI18N
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, -1));

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btnUpdateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUpdateActionPerformed
		// TODO add your handling code here:
		int i = Table.getSelectedRow();
		if (i == -1) {
			JOptionPane.showMessageDialog(this, "Please choose a record!", "Warning", JOptionPane.ERROR_MESSAGE, warningIcon);
		} else {
			if (lblJudul.getText().equals("Expertise Data")) {
				KeahlianUI keahlian = new KeahlianUI();
				keahlian.btnInsert.setText("Update");
				keahlian.setVisible(true);
				keahlian.txtNamaKeahlian.setText(Table.getValueAt(i, 1).toString());
				keahlian.txtKetKeahlian.setText(Table.getValueAt(i, 2).toString());
				keahlian.passTable(Table, i);
			}
			if (lblJudul.getText().equals("Position Data")) {
				PangkatUI pangkat = new PangkatUI();
				pangkat.btnInsert.setText("Update");
				pangkat.setVisible(true);
				pangkat.txtNamaPangkat.setText(Table.getValueAt(i, 1).toString());
				pangkat.txtKetPangkat.setText(Table.getValueAt(i, 2).toString());
				pangkat.passTable(Table, i);
			}
			if (lblJudul.getText().equals("Division Data")) {
				DivisiUI divisi = new DivisiUI();
				divisi.btnInsert.setText("Update");
				divisi.setVisible(true);
				divisi.txtNama.setText(Table.getValueAt(i, 1).toString());
				divisi.txtBidang.setText(Table.getValueAt(i, 2).toString());
				divisi.passTable(Table, i);
			}
			if (lblJudul.getText().equals("Working Program Data")) {
				ProkerUI proker = new ProkerUI();
				proker.btnInsert.setText("Update");
				proker.setVisible(true);
				proker.txtNamaProker.setText(Table.getValueAt(i, 1).toString());
				proker.txtKetProker.setText(Table.getValueAt(i, 2).toString());
				proker.passTable(Table, i);
			}
			if (lblJudul.getText().equals("Employee Data")) {
				KaryawanUI karyawan = new KaryawanUI();
				karyawan.btnInsert.setText("Update");
				karyawan.setVisible(true);
				karyawan.txtNamaKaryawan.setText(Table.getValueAt(i, 1).toString());
				karyawan.txtAlamat.setText(Table.getValueAt(i, 2).toString());
				karyawan.dpTglLahir.setText(Table.getValueAt(i, 3).toString());
				karyawan.cbDivisi.setSelectedItem(0);
				karyawan.cbKeahlian.setSelectedItem(0);
				karyawan.passTable(Table, i);
			}
			if (lblJudul.getText().equals("Employee's Position Data")) {
				PlotPKUI jabatan = new PlotPKUI();
				jabatan.btnInsert.setText("Update");
				jabatan.setVisible(true);
				jabatan.cbKaryawan.setSelectedIndex(0);
				jabatan.cbPangkat.setSelectedIndex(0);
				jabatan.dpJabatan.setText(Table.getValueAt(i, 2).toString());
				jabatan.passTable(Table, i);
			}
			if (lblJudul.getText().equals("Head of Division Data")) {
				PlotKadivUI kadiv = new PlotKadivUI();
				kadiv.btnInsert.setText("Update");
				kadiv.setVisible(true);
				kadiv.cbDivisi.setSelectedIndex(0);
				kadiv.cbKadiv.setSelectedIndex(0);;
				kadiv.passTable(Table, i);
			}
			if (lblJudul.getText().equals("Supervisor Data")) {
				PlotSpvUI spv = new PlotSpvUI();
				spv.btnInsert.setText("Update");
				spv.setVisible(true);
				spv.cbSpv.setSelectedIndex(0);
				spv.cbKaryawan.setSelectedIndex(0);
				spv.passTable(Table, i);
			}
			if (lblJudul.getText().equals("Division Superintendent Data")) {
				PlotPawasUI pengawas = new PlotPawasUI();
				pengawas.btnInsert.setText("Update");
				pengawas.setVisible(true);
				pengawas.cbDivisi.setSelectedIndex(0);
				pengawas.cbPengawas.setSelectedIndex(0);
				pengawas.passTable(Table, i);
			}
			if (lblJudul.getText().equals("Working Program Plan Data")) {
				PlotProkerUI rencana = new PlotProkerUI();
				rencana.btnInsert.setText("Update");
				rencana.setVisible(true);
				rencana.cbProker.setSelectedIndex(0);
				rencana.cbDivisi.setSelectedIndex(0);
				rencana.dpPelaksanaan.setText(Table.getValueAt(i, 2).toString());
				rencana.passTable(Table, i);
			}
		}

    }//GEN-LAST:event_btnUpdateActionPerformed

    private void txtSearchFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtSearchFocusGained
		// TODO add your handling code here:
		if (txtSearch.getText().equals("Enter Attribute. . .")) {
			txtSearch.setText("");
			txtSearch.setForeground(Color.BLACK);
		}
    }//GEN-LAST:event_txtSearchFocusGained

    private void txtSearchFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtSearchFocusLost
		// TODO add your handling code here:
		if (txtSearch.getText().equals("")) {
			txtSearch.setText("Enter Attribute. . .");
			txtSearch.setForeground(Color.GRAY);
		}
    }//GEN-LAST:event_txtSearchFocusLost

    private void txtSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtSearchActionPerformed
		// TODO add your handling code here:
    }//GEN-LAST:event_txtSearchActionPerformed

    private void btnSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSearchActionPerformed
		// TODO add your handling code here:
		if (cbAttribute.getSelectedIndex() == 0) {
			JOptionPane.showMessageDialog(this, "Please choose an attribute!", "Warning", JOptionPane.ERROR_MESSAGE, warningIcon);
			setLabel(label);
		} else {

			try {
				if ("Division Data".equals(label)) {
					if (txtSearch.getText().equals("") || txtSearch.getText().equals("Enter Attribute. . .")) {
						rs = stm.executeQuery("SELECT COUNT(*) FROM Divisi WHERE deleted=0");
					} else {
						switch (cbAttribute.getSelectedIndex()) {
							case 1:
								rs = stm.executeQuery("SELECT COUNT(*) FROM Divisi WHERE deleted=0 and IDDivisi like '%" + txtSearch.getText() + "%'");
								break;
							case 2:
								rs = stm.executeQuery("SELECT COUNT(*) FROM Divisi WHERE deleted=0 and NamaDivisi like '%" + txtSearch.getText() + "%'");
								break;
							case 3:
								rs = stm.executeQuery("SELECT COUNT(*) FROM Divisi WHERE deleted=0 and Bidang like '%" + txtSearch.getText() + "%'");
								break;
							default:
								rs = stm.executeQuery("SELECT COUNT(*) FROM Divisi WHERE deleted=0");
								break;
						}
					}
					rs.next();
					int row = rs.getInt(1);
					int col = 3;
					String[] colName = {"ID Divisi", "Nama Divisi", "Bidang"};
					String[][] data = new String[row][col];
					if (txtSearch.getText().equals("") || txtSearch.getText().equals("Enter Attribute. . .")) {
						rs = stm.executeQuery("SELECT IDDivisi, NamaDivisi, Bidang FROM Divisi WHERE deleted=0");
					} else {
						switch (cbAttribute.getSelectedIndex()) {
							case 1:
								rs = stm.executeQuery("SELECT IDDivisi, NamaDivisi, Bidang FROM Divisi WHERE deleted=0 and IDDivisi like '%" + txtSearch.getText() + "%'");
								break;
							case 2:
								rs = stm.executeQuery("SELECT IDDivisi, NamaDivisi, Bidang FROM Divisi WHERE deleted=0 and NamaDivisi like '%" + txtSearch.getText() + "%'");
								break;
							case 3:
								rs = stm.executeQuery("SELECT IDDivisi, NamaDivisi, Bidang FROM Divisi WHERE deleted=0 and Bidang like '%" + txtSearch.getText() + "%'");
								break;
							default:
								rs = stm.executeQuery("SELECT IDDivisi, NamaDivisi, Bidang FROM Divisi WHERE deleted=0");
								break;
						}
					}
					int i = 0;
					while (rs.next()) {
						for (int j = 0; j < col; j++) {
							data[i][j] = rs.getString(j + 1);
						}
						i++;
					}
					ShowData(colName, data);
				}
				if ("Employee Data".equals(label)) {
					if (txtSearch.getText().equals("") || txtSearch.getText().equals("Enter Attribute. . .")) {
						rs = stm.executeQuery("SELECT COUNT(*) FROM Karyawan " +
									"LEFT JOIN Divisi ON Divisi.IDDivisi = Karyawan.IDMenempati and Divisi.deleted=0 " +
									"LEFT JOIN Keahlian ON Keahlian.IDKeahlian = Karyawan.IDKeahlian and Keahlian.deleted=0 " +
									"WHERE Karyawan.deleted=0");
					} else {
						switch (cbAttribute.getSelectedIndex()) {
							case 1:
								rs = stm.executeQuery("SELECT COUNT(*) FROM Karyawan " +
									"LEFT JOIN Divisi ON Divisi.IDDivisi = Karyawan.IDMenempati and Divisi.deleted=0 " +
									"LEFT JOIN Keahlian ON Keahlian.IDKeahlian = Karyawan.IDKeahlian and Keahlian.deleted=0 " +
									"WHERE Karyawan.deleted=0 and IDKaryawan like '%" + txtSearch.getText() + "%'");
								break;
							case 2:
								rs = stm.executeQuery("SELECT COUNT(*) FROM Karyawan " +
									"LEFT JOIN Divisi ON Divisi.IDDivisi = Karyawan.IDMenempati and Divisi.deleted=0 " +
									"LEFT JOIN Keahlian ON Keahlian.IDKeahlian = Karyawan.IDKeahlian and Keahlian.deleted=0 " +
									"WHERE Karyawan.deleted=0 and Nama like '%" + txtSearch.getText() + "%'");
								break;
							case 3:
								rs = stm.executeQuery("SELECT COUNT(*) FROM Karyawan " +
									"LEFT JOIN Divisi ON Divisi.IDDivisi = Karyawan.IDMenempati and Divisi.deleted=0 " +
									"LEFT JOIN Keahlian ON Keahlian.IDKeahlian = Karyawan.IDKeahlian and Keahlian.deleted=0 " +
									"WHERE Karyawan.deleted=0 and Alamat like '%" + txtSearch.getText() + "%'");
								break;
							case 4:
								rs = stm.executeQuery("SELECT COUNT(*) FROM Karyawan " +
									"LEFT JOIN Divisi ON Divisi.IDDivisi = Karyawan.IDMenempati and Divisi.deleted=0 " +
									"LEFT JOIN Keahlian ON Keahlian.IDKeahlian = Karyawan.IDKeahlian and Keahlian.deleted=0 " +
									"WHERE Karyawan.deleted=0 and TglLahir like '%" + txtSearch.getText() + "%'");
								break;
							case 5:
								rs = stm.executeQuery("SELECT COUNT(*) FROM Karyawan " +
									"LEFT JOIN Divisi ON Divisi.IDDivisi = Karyawan.IDMenempati and Divisi.deleted=0 " +
									"LEFT JOIN Keahlian ON Keahlian.IDKeahlian = Karyawan.IDKeahlian and Keahlian.deleted=0 " +
									"WHERE Karyawan.deleted=0 and NamaDivisi like '%" + txtSearch.getText() + "%'");
								break;
							case 6:
								rs = stm.executeQuery("SELECT COUNT(*) FROM Karyawan " +
									"LEFT JOIN Divisi ON Divisi.IDDivisi = Karyawan.IDMenempati and Divisi.deleted=0 " +
									"LEFT JOIN Keahlian ON Keahlian.IDKeahlian = Karyawan.IDKeahlian and Keahlian.deleted=0 " +
									"WHERE Karyawan.deleted=0 and NamaKeahlian like '%" + txtSearch.getText() + "%'");
								break;
							default:
								rs = stm.executeQuery("SELECT COUNT(*) FROM Karyawan " +
									"LEFT JOIN Divisi ON Divisi.IDDivisi = Karyawan.IDMenempati and Divisi.deleted=0 " +
									"LEFT JOIN Keahlian ON Keahlian.IDKeahlian = Karyawan.IDKeahlian and Keahlian.deleted=0 " +
									"WHERE Karyawan.deleted=0");
								break;
						}
					}
					rs.next();
					int row = rs.getInt(1);
					int col = 6;
					String[] colName = {"ID Karyawan", "Nama", "Alamat", "Tanggal Lahir", "Divisi", "Keahlian"};
					String[][] data = new String[row][col];
					if (txtSearch.getText().equals("") || txtSearch.getText().equals("Enter Attribute. . .")) {
						rs = stm.executeQuery("SELECT IDKaryawan, Nama, Alamat, TglLahir, NamaDivisi, NamaKeahlian FROM Karyawan " +
									"LEFT JOIN Divisi ON Divisi.IDDivisi = Karyawan.IDMenempati and Divisi.deleted=0 " +
									"LEFT JOIN Keahlian ON Keahlian.IDKeahlian = Karyawan.IDKeahlian and Keahlian.deleted=0 " +
									"WHERE Karyawan.deleted=0");
					}else{
						switch (cbAttribute.getSelectedIndex()) {
							case 1:
								rs = stm.executeQuery("SELECT IDKaryawan, Nama, Alamat, TglLahir, NamaDivisi, NamaKeahlian FROM Karyawan " +
									"LEFT JOIN Divisi ON Divisi.IDDivisi = Karyawan.IDMenempati and Divisi.deleted=0 " +
									"LEFT JOIN Keahlian ON Keahlian.IDKeahlian = Karyawan.IDKeahlian and Keahlian.deleted=0 " +
									"WHERE Karyawan.deleted=0 and IDKaryawan like '%" + txtSearch.getText() + "%'");
								break;
							case 2:
								rs = stm.executeQuery("SELECT IDKaryawan, Nama, Alamat, TglLahir, NamaDivisi, NamaKeahlian FROM Karyawan " +
									"LEFT JOIN Divisi ON Divisi.IDDivisi = Karyawan.IDMenempati and Divisi.deleted=0 " +
									"LEFT JOIN Keahlian ON Keahlian.IDKeahlian = Karyawan.IDKeahlian and Keahlian.deleted=0 " +
									"WHERE Karyawan.deleted=0 and Nama like '%" + txtSearch.getText() + "%'");
								break;
							case 3:
								rs = stm.executeQuery("SELECT IDKaryawan, Nama, Alamat, TglLahir, NamaDivisi, NamaKeahlian FROM Karyawan " +
									"LEFT JOIN Divisi ON Divisi.IDDivisi = Karyawan.IDMenempati and Divisi.deleted=0 " +
									"LEFT JOIN Keahlian ON Keahlian.IDKeahlian = Karyawan.IDKeahlian and Keahlian.deleted=0 " +
									"WHERE Karyawan.deleted=0 and Alamat like '%" + txtSearch.getText() + "%'");
								break;
							case 4:
								rs = stm.executeQuery("SELECT IDKaryawan, Nama, Alamat, TglLahir, NamaDivisi, NamaKeahlian FROM Karyawan " +
									"LEFT JOIN Divisi ON Divisi.IDDivisi = Karyawan.IDMenempati and Divisi.deleted=0 " +
									"LEFT JOIN Keahlian ON Keahlian.IDKeahlian = Karyawan.IDKeahlian and Keahlian.deleted=0 " +
									"WHERE Karyawan.deleted=0 and TglLahir like '%" + txtSearch.getText() + "%'");
								break;
							case 5:
								rs = stm.executeQuery("SELECT IDKaryawan, Nama, Alamat, TglLahir, NamaDivisi, NamaKeahlian FROM Karyawan " +
									"LEFT JOIN Divisi ON Divisi.IDDivisi = Karyawan.IDMenempati and Divisi.deleted=0 " +
									"LEFT JOIN Keahlian ON Keahlian.IDKeahlian = Karyawan.IDKeahlian and Keahlian.deleted=0 " +
									"WHERE Karyawan.deleted=0 and NamaDivisi like '%" + txtSearch.getText() + "%'");
								break;
							case 6:
								rs = stm.executeQuery("SELECT IDKaryawan, Nama, Alamat, TglLahir, NamaDivisi, NamaKeahlian FROM Karyawan " +
									"LEFT JOIN Divisi ON Divisi.IDDivisi = Karyawan.IDMenempati and Divisi.deleted=0 " +
									"LEFT JOIN Keahlian ON Keahlian.IDKeahlian = Karyawan.IDKeahlian and Keahlian.deleted=0 " +
									"WHERE Karyawan.deleted=0 and NamaKeahlian like '%" + txtSearch.getText() + "%'");
								break;
							default:
								rs = stm.executeQuery("SELECT IDKaryawan, Nama, Alamat, TglLahir, NamaDivisi, NamaKeahlian FROM Karyawan " +
									"LEFT JOIN Divisi ON Divisi.IDDivisi = Karyawan.IDMenempati and Divisi.deleted=0 " +
									"LEFT JOIN Keahlian ON Keahlian.IDKeahlian = Karyawan.IDKeahlian and Keahlian.deleted=0 " +
									"WHERE Karyawan.deleted=0");
								break;
						}
					}
					int i = 0;
					while (rs.next()) {
						for (int j = 0; j < col; j++) {
							data[i][j] = rs.getString(j + 1);
						}
						i++;
					}
					ShowData(colName, data);
				}
				if ("Expertise Data".equals(label)) {
					if (txtSearch.getText().equals("") || txtSearch.getText().equals("Enter Attribute. . .")) {
						rs = stm.executeQuery("SELECT COUNT(*) FROM Keahlian WHERE deleted=0");
					} else {
						switch (cbAttribute.getSelectedIndex()) {
							case 1:
								rs = stm.executeQuery("SELECT COUNT(*) FROM Keahlian WHERE deleted=0 and IDKeahlian like '%" + txtSearch.getText() + "%'");
								break;
							case 2:
								rs = stm.executeQuery("SELECT COUNT(*) FROM Keahlian WHERE deleted=0 and NamaKeahlian like '%" + txtSearch.getText() + "%'");
								break;
							case 3:
								rs = stm.executeQuery("SELECT COUNT(*) FROM Keahlian WHERE deleted=0 and Keterangan like '%" + txtSearch.getText() + "%'");
								break;
							default:
								rs = stm.executeQuery("SELECT COUNT(*) FROM Keahlian WHERE deleted=0");
								break;
						}
					}
					rs.next();
					int row = rs.getInt(1);
					int col = 3;
					String[] colName = {"ID Keahlian", "Nama Keahlian", "Keterangan"};
					String[][] data = new String[row][col];
					if (txtSearch.getText().equals("") || txtSearch.getText().equals("Enter Attribute. . .")) {
						rs = stm.executeQuery("SELECT IDKeahlian, NamaKeahlian, Keterangan FROM Keahlian WHERE deleted=0");
					} else {
						switch (cbAttribute.getSelectedIndex()) {
							case 1:
								rs = stm.executeQuery("SELECT IDKeahlian, NamaKeahlian, Keterangan FROM Keahlian WHERE deleted=0 and IDKeahlian like '%" + txtSearch.getText() + "%'");
								break;
							case 2:
								rs = stm.executeQuery("SELECT IDKeahlian, NamaKeahlian, Keterangan FROM Keahlian WHERE deleted=0 and NamaKeahlian like '%" + txtSearch.getText() + "%'");
								break;
							case 3:
								rs = stm.executeQuery("SELECT IDKeahlian, NamaKeahlian, Keterangan FROM Keahlian WHERE deleted=0 and Keterangan like '%" + txtSearch.getText() + "%'");
								break;
							default:
								rs = stm.executeQuery("SELECT IDKeahlian, NamaKeahlian, Keterangan FROM Keahlian WHERE deleted=0");
								break;
						}
					}
					int i = 0;
					while (rs.next()) {
						for (int j = 0; j < col; j++) {
							data[i][j] = rs.getString(j + 1);
						}
						i++;
					}
					ShowData(colName, data);
				}
				if ("Position Data".equals(label)) {
					if (txtSearch.getText().equals("") || txtSearch.getText().equals("Enter Attribute. . .")) {
						rs = stm.executeQuery("SELECT COUNT(*) FROM Pangkat WHERE deleted=0");
					} else {
						switch (cbAttribute.getSelectedIndex()) {
							case 1:
								rs = stm.executeQuery("SELECT COUNT(*) FROM Pangkat WHERE deleted=0 and IDPangkat like '%" + txtSearch.getText() + "%'");
								break;
							case 2:
								rs = stm.executeQuery("SELECT COUNT(*) FROM Pangkat WHERE deleted=0 and NamaKepangkatan like '%" + txtSearch.getText() + "%'");
								break;
							case 3:
								rs = stm.executeQuery("SELECT COUNT(*) FROM Pangkat WHERE deleted=0 and Keterangan like '%" + txtSearch.getText() + "%'");
								break;
							default:
								rs = stm.executeQuery("SELECT COUNT(*) FROM Pangkat WHERE deleted=0");
								break;
						}
					}
					rs.next();
					int row = rs.getInt(1);
					int col = 3;
					String[] colName = {"ID Pangkat", "Nama Kepangkatan", "Keterangan"};
					String[][] data = new String[row][col];
					if (txtSearch.getText().equals("") || txtSearch.getText().equals("Enter Attribute. . .")) {
						rs = stm.executeQuery("SELECT IDPangkat, NamaKepangkatan, Keterangan FROM Pangkat WHERE deleted=0");
					} else {
						switch (cbAttribute.getSelectedIndex()) {
							case 1:
								rs = stm.executeQuery("SELECT IDPangkat, NamaKepangkatan, Keterangan FROM Pangkat WHERE deleted=0 and IDPangkat like '%" + txtSearch.getText() + "%'");
								break;
							case 2:
								rs = stm.executeQuery("SELECT IDPangkat, NamaKepangkatan, Keterangan FROM Pangkat WHERE deleted=0 and NamaKepangkatan like '%" + txtSearch.getText() + "%'");
								break;
							case 3:
								rs = stm.executeQuery("SELECT IDPangkat, NamaKepangkatan, Keterangan FROM Pangkat WHERE deleted=0 and Keterangan like '%" + txtSearch.getText() + "%'");
								break;
							default:
								rs = stm.executeQuery("SELECT IDPangkat, NamaKepangkatan, Keterangan FROM Pangkat WHERE deleted=0");
								break;
						}
					}
					int i = 0;
					while (rs.next()) {
						for (int j = 0; j < col; j++) {
							data[i][j] = rs.getString(j + 1);
						}
						i++;
					}
					ShowData(colName, data);
				}
				if ("Working Program Data".equals(label)) {
					if (txtSearch.getText().equals("") || txtSearch.getText().equals("Enter Attribute. . .")) {
						rs = stm.executeQuery("SELECT COUNT(*) FROM ProgramKerja WHERE deleted=0");
					} else {
						switch (cbAttribute.getSelectedIndex()) {
							case 1:
								rs = stm.executeQuery("SELECT COUNT(*) FROM ProgramKerja WHERE deleted=0 and IDProKer like '%" + txtSearch.getText() + "%'");
								break;
							case 2:
								rs = stm.executeQuery("SELECT COUNT(*) FROM ProgramKerja WHERE deleted=0 and NamaProKer like '%" + txtSearch.getText() + "%'");
								break;
							case 3:
								rs = stm.executeQuery("SELECT COUNT(*) FROM ProgramKerja WHERE deleted=0 and Keterangan like '%" + txtSearch.getText() + "%'");
								break;
							default:
								rs = stm.executeQuery("SELECT COUNT(*) FROM ProgramKerja WHERE deleted=0");
								break;
						}
					}
					rs.next();
					int row = rs.getInt(1);
					int col = 3;
					String[] colName = {"ID ProKer", "Nama ProKer", "Keterangan"};
					String[][] data = new String[row][col];
					if (txtSearch.getText().equals("") || txtSearch.getText().equals("Enter Attribute. . .")) {
						rs = stm.executeQuery("SELECT IDProKer, NamaProker, Keterangan FROM ProgramKerja WHERE deleted=0");
					} else {
						switch (cbAttribute.getSelectedIndex()) {
							case 1:
								rs = stm.executeQuery("SELECT IDProKer, NamaProker, Keterangan FROM ProgramKerja WHERE deleted=0 and IDProKer like '%" + txtSearch.getText() + "%'");
								break;
							case 2:
								rs = stm.executeQuery("SELECT IDProKer, NamaProker, Keterangan FROM ProgramKerja WHERE deleted=0 and NamaProKer like '%" + txtSearch.getText() + "%'");
								break;
							case 3:
								rs = stm.executeQuery("SELECT IDProKer, NamaProker, Keterangan FROM ProgramKerja WHERE deleted=0 and Keterangan like '%" + txtSearch.getText() + "%'");
								break;
							default:
								rs = stm.executeQuery("SELECT IDProKer, NamaProker, Keterangan FROM ProgramKerja WHERE deleted=0");
								break;
						}
					}
					int i = 0;
					while (rs.next()) {
						for (int j = 0; j < col; j++) {
							data[i][j] = rs.getString(j + 1);
						}
						i++;
					}
					ShowData(colName, data);
				}
				if ("Working Program Plan Data".equals(label)) {
					if (txtSearch.getText().equals("") || txtSearch.getText().equals("Enter Attribute. . .")) {
						rs = stm.executeQuery("SELECT COUNT(*) FROM Rencana " +
									"LEFT JOIN ProgramKerja PK ON PK.IDProKer=Rencana.IDProKer and PK.deleted=0 " +
									"LEFT JOIN Divisi ON Divisi.IDDivisi=Rencana.IDDivisi and Divisi.deleted=0 " +
									"WHERE Divisi.deleted=0 or PK.deleted=0");
					} else {
						switch (cbAttribute.getSelectedIndex()) {
							case 1:
								rs = stm.executeQuery("SELECT COUNT(*) FROM Rencana " +
									"LEFT JOIN ProgramKerja PK ON PK.IDProKer=Rencana.IDProKer and PK.deleted=0 " +
									"LEFT JOIN Divisi ON Divisi.IDDivisi=Rencana.IDDivisi and Divisi.deleted=0 " +
									"WHERE Divisi.deleted=0 or PK.deleted=0 and NamaProker like '%" + txtSearch.getText() + "%'");
								break;
							case 2:
								rs = stm.executeQuery("SELECT COUNT(*) FROM Rencana " +
									"LEFT JOIN ProgramKerja PK ON PK.IDProKer=Rencana.IDProKer and PK.deleted=0 " +
									"LEFT JOIN Divisi ON Divisi.IDDivisi=Rencana.IDDivisi and Divisi.deleted=0 " +
									"WHERE Divisi.deleted=0 or PK.deleted=0 and NamaDivisi like '%" + txtSearch.getText() + "%'");
								break;
							case 3:
								rs = stm.executeQuery("SELECT COUNT(*) FROM Rencana " +
									"LEFT JOIN ProgramKerja PK ON PK.IDProKer=Rencana.IDProKer and PK.deleted=0 " +
									"LEFT JOIN Divisi ON Divisi.IDDivisi=Rencana.IDDivisi and Divisi.deleted=0 " +
									"WHERE Divisi.deleted=0 or PK.deleted=0 and TglPelaksanaan like '%" + txtSearch.getText() + "%'");
								break;
							default:
								rs = stm.executeQuery("SELECT COUNT(*) FROM Rencana " +
									"LEFT JOIN ProgramKerja PK ON PK.IDProKer=Rencana.IDProKer and PK.deleted=0 " +
									"LEFT JOIN Divisi ON Divisi.IDDivisi=Rencana.IDDivisi and Divisi.deleted=0 " +
									"WHERE Divisi.deleted=0 or PK.deleted=0");
								break;
						}
					}
					rs.next();
					int row = rs.getInt(1);
					int col = 3;
					String[] colName = {"Program Kerja", "Divisi", "Tanggal Pelaksanaa"};
					String[][] data = new String[row][col];
					if (txtSearch.getText().equals("") || txtSearch.getText().equals("Enter Attribute. . .")) {
						rs = stm.executeQuery("SELECT NamaDivisi, NamaProker, TglPelaksanaan FROM Rencana " +
									"LEFT JOIN ProgramKerja PK ON PK.IDProKer=Rencana.IDProKer and PK.deleted=0 " +
									"LEFT JOIN Divisi ON Divisi.IDDivisi=Rencana.IDDivisi and Divisi.deleted=0 " +
									"WHERE Divisi.deleted=0 or PK.deleted=0");
					} else {
						switch (cbAttribute.getSelectedIndex()) {
							case 1:
								rs = stm.executeQuery("SELECT NamaDivisi, NamaProker, TglPelaksanaan FROM Rencana " +
									"LEFT JOIN ProgramKerja PK ON PK.IDProKer=Rencana.IDProKer and PK.deleted=0 " +
									"LEFT JOIN Divisi ON Divisi.IDDivisi=Rencana.IDDivisi and Divisi.deleted=0 " +
									"WHERE Divisi.deleted=0 or PK.deleted=0 and NamaProker like '%" + txtSearch.getText() + "%'");
								break;
							case 2:
								rs = stm.executeQuery("SELECT NamaDivisi, NamaProker, TglPelaksanaan FROM Rencana " +
									"LEFT JOIN ProgramKerja PK ON PK.IDProKer=Rencana.IDProKer and PK.deleted=0 " +
									"LEFT JOIN Divisi ON Divisi.IDDivisi=Rencana.IDDivisi and Divisi.deleted=0 " +
									"WHERE Divisi.deleted=0 or PK.deleted=0 and NamaDivisi like '%" + txtSearch.getText() + "%'");
								break;
							case 3:
								rs = stm.executeQuery("SELECT NamaDivisi, NamaProker, TglPelaksanaan FROM Rencana " +
									"LEFT JOIN ProgramKerja PK ON PK.IDProKer=Rencana.IDProKer and PK.deleted=0 " +
									"LEFT JOIN Divisi ON Divisi.IDDivisi=Rencana.IDDivisi and Divisi.deleted=0 " +
									"WHERE Divisi.deleted=0 or PK.deleted=0 and TglPelaksanaan like '%" + txtSearch.getText() + "%'");
								break;
							default:
								rs = stm.executeQuery("SELECT NamaDivisi, NamaProker, TglPelaksanaan FROM Rencana " +
									"LEFT JOIN ProgramKerja PK ON PK.IDProKer=Rencana.IDProKer and PK.deleted=0 " +
									"LEFT JOIN Divisi ON Divisi.IDDivisi=Rencana.IDDivisi and Divisi.deleted=0 " +
									"WHERE Divisi.deleted=0 or PK.deleted=0");
								break;
						}
					}
					int i = 0;
					while (rs.next()) {
						for (int j = 0; j < col; j++) {
							data[i][j] = rs.getString(j + 1);
						}
						i++;
					}
					ShowData(colName, data);
				}
				if ("Supervisor Data".equals(label)) {
					if (txtSearch.getText().equals("") || txtSearch.getText().equals("Enter Attribute. . .")) {
						rs = stm.executeQuery("SELECT COUNT(*) FROM Karyawan S,Karyawan A " +
									"WHERE S.IDKaryawan=A.IDSupervisor and A.deleted=0 and S.deleted=0");
					} else {
						switch (cbAttribute.getSelectedIndex()) {
							case 1:
								rs = stm.executeQuery("SELECT COUNT(*) FROM Karyawan S,Karyawan A " +
									"WHERE S.IDKaryawan=A.IDSupervisor and A.deleted=0 and S.deleted=0 and S.Nama like '%" + txtSearch.getText() + "%'");
								break;
							case 2:
								rs = stm.executeQuery("SELECT COUNT(*) FROM Karyawan S,Karyawan A " +
									"WHERE S.IDKaryawan=A.IDSupervisor and A.deleted=0 and S.deleted=0 and A.Nama like '%" + txtSearch.getText() + "%'");
								break;
							default:
								rs = stm.executeQuery("SELECT COUNT(*) FROM Karyawan S,Karyawan A " +
									"WHERE S.IDKaryawan=A.IDSupervisor and A.deleted=0 and S.deleted=0");
								break;
						}
					}
					rs.next();
					int row = rs.getInt(1);
					int col = 2;
					String[] colName = {"Nama Supervisor", "Nama Karyawan"};
					String[][] data = new String[row][col];
					if (txtSearch.getText().equals("") || txtSearch.getText().equals("Enter Attribute. . .")) {
						rs = stm.executeQuery("SELECT S.Nama, A.Nama FROM Karyawan S,Karyawan A " +
									"WHERE S.IDKaryawan=A.IDSupervisor and A.deleted=0 and S.deleted=0");
					} else {
						switch (cbAttribute.getSelectedIndex()) {
							case 1:
								rs = stm.executeQuery("SELECT S.Nama, A.Nama FROM Karyawan S,Karyawan A " +
									"WHERE S.IDKaryawan=A.IDSupervisor and A.deleted=0 and S.deleted=0 and S.Nama like '%" + txtSearch.getText() + "%'");
								break;
							case 2:
								rs = stm.executeQuery("SELECT S.Nama, A.Nama FROM Karyawan S,Karyawan A " +
									"WHERE S.IDKaryawan=A.IDSupervisor and A.deleted=0 and S.deleted=0 and A.Nama like '%" + txtSearch.getText() + "%'");
								break;
							default:
								rs = stm.executeQuery("SELECT S.Nama, A.Nama FROM Karyawan S,Karyawan A " +
									"WHERE S.IDKaryawan=A.IDSupervisor and A.deleted=0 and S.deleted=0");
								break;
						}
					}
					int i = 0;
					while (rs.next()) {
						for (int j = 0; j < col; j++) {
							data[i][j] = rs.getString(j + 1);
						}
						i++;
					}
					ShowData(colName, data);
				}
				if ("Division Superintendent Data".equals(label)) {
					if (txtSearch.getText().equals("") || txtSearch.getText().equals("Enter Attribute. . .")) {
						rs = stm.executeQuery("SELECT COUNT(*) FROM Divisi " +
									"LEFT JOIN Karyawan ON Divisi.IDDivisi=Karyawan.IDMengawas and Karyawan.deleted=0 " +
									"WHERE Divisi.deleted=0");
					} else {
						switch (cbAttribute.getSelectedIndex()) {
							case 1:
								rs = stm.executeQuery("SELECT COUNT(*) FROM Divisi " +
									"LEFT JOIN Karyawan ON Divisi.IDDivisi=Karyawan.IDMengawas and Karyawan.deleted=0 " +
									"WHERE Divisi.deleted=0 and NamaDivisi like '%" + txtSearch.getText() + "%'");
								break;
							case 2:
								rs = stm.executeQuery("SELECT COUNT(*) FROM Divisi " +
									"LEFT JOIN Karyawan ON Divisi.IDDivisi=Karyawan.IDMengawas and Karyawan.deleted=0 " +
									"WHERE Divisi.deleted=0 and Nama like '%" + txtSearch.getText() + "%'");
								break;
							default:
								rs = stm.executeQuery("SELECT COUNT(*) FROM Divisi " +
									"LEFT JOIN Karyawan ON Divisi.IDDivisi=Karyawan.IDMengawas and Karyawan.deleted=0 " +
									"WHERE Divisi.deleted=0");
								break;
						}
					}
					rs.next();
					int row = rs.getInt(1);
					int col = 2;
					String[] colName = {"Divisi", "Pengawas Divisi"};
					String[][] data = new String[row][col];
					if (txtSearch.getText().equals("") || txtSearch.getText().equals("Enter Attribute. . .")) {
						rs = stm.executeQuery("SELECT NamaDivisi, Nama FROM Divisi " +
									"LEFT JOIN Karyawan ON Divisi.IDDivisi=Karyawan.IDMengawas and Karyawan.deleted=0 " +
									"WHERE Divisi.deleted=0");
					} else {
						switch (cbAttribute.getSelectedIndex()) {
							case 1:
								rs = stm.executeQuery("SELECT NamaDivisi, Nama FROM Divisi " +
									"LEFT JOIN Karyawan ON Divisi.IDDivisi=Karyawan.IDMengawas and Karyawan.deleted=0 " +
									"WHERE Divisi.deleted=0 and NamaDivisi like '%" + txtSearch.getText() + "%'");
								break;
							case 2:
								rs = stm.executeQuery("SELECT NamaDivisi, Nama FROM Divisi " +
									"LEFT JOIN Karyawan ON Divisi.IDDivisi=Karyawan.IDMengawas and Karyawan.deleted=0 " +
									"WHERE Divisi.deleted=0 and Nama like '%" + txtSearch.getText() + "%'");
								break;
							default:
								rs = stm.executeQuery("SELECT NamaDivisi, Nama FROM Divisi " +
									"LEFT JOIN Karyawan ON Divisi.IDDivisi=Karyawan.IDMengawas and Karyawan.deleted=0 " +
									"WHERE Divisi.deleted=0");
								break;
						}
					}
					int i = 0;
					while (rs.next()) {
						for (int j = 0; j < col; j++) {
							data[i][j] = rs.getString(j + 1);
						}
						i++;
					}
					ShowData(colName, data);
				}
				if ("Head of Division Data".equals(label)) {
					if (txtSearch.getText().equals("") || txtSearch.getText().equals("Enter Attribute. . .")) {
						rs = stm.executeQuery("SELECT COUNT(*) FROM Divisi " +
									"LEFT JOIN Karyawan ON Divisi.IDKepala=Karyawan.IDKaryawan and Karyawan.deleted=0 " +
									"WHERE Divisi.deleted=0");
					} else {
						switch (cbAttribute.getSelectedIndex()) {
							case 1:
								rs = stm.executeQuery("SELECT COUNT(*) FROM Divisi " +
									"LEFT JOIN Karyawan ON Divisi.IDKepala=Karyawan.IDKaryawan and Karyawan.deleted=0 " +
									"WHERE Divisi.deleted=0 and NamaDivisi like '%" + txtSearch.getText() + "%'");
								break;
							case 2:
								rs = stm.executeQuery("SELECT COUNT(*) FROM Divisi " +
									"LEFT JOIN Karyawan ON Divisi.IDKepala=Karyawan.IDKaryawan and Karyawan.deleted=0 " +
									"WHERE Divisi.deleted=0 and Nama like '%" + txtSearch.getText() + "%'");
								break;
							default:
								rs = stm.executeQuery("SELECT COUNT(*) FROM Divisi " +
									"LEFT JOIN Karyawan ON Divisi.IDKepala=Karyawan.IDKaryawan and Karyawan.deleted=0 " +
									"WHERE Divisi.deleted=0");
								break;
						}
					}
					rs.next();
					int row = rs.getInt(1);
					int col = 2;
					String[] colName = {"Divisi", "Kepala Divisi"};
					String[][] data = new String[row][col];
					if (txtSearch.getText().equals("") || txtSearch.getText().equals("Enter Attribute. . .")) {
						rs = stm.executeQuery("SELECT NamaDivisi, Nama FROM Divisi " +
									"LEFT JOIN Karyawan ON Divisi.IDKepala=Karyawan.IDKaryawan and Karyawan.deleted=0 " +
									"WHERE Divisi.deleted=0");
					} else {
						switch (cbAttribute.getSelectedIndex()) {
							case 1:
								rs = stm.executeQuery("SELECT NamaDivisi, Nama FROM Divisi " +
									"LEFT JOIN Karyawan ON Divisi.IDKepala=Karyawan.IDKaryawan and Karyawan.deleted=0 " +
									"WHERE Divisi.deleted=0 and NamaDivisi like '%" + txtSearch.getText() + "%'");
								break;
							case 2:
								rs = stm.executeQuery("SELECT NamaDivisi, Nama FROM Divisi " +
									"LEFT JOIN Karyawan ON Divisi.IDKepala=Karyawan.IDKaryawan and Karyawan.deleted=0 " +
									"WHERE Divisi.deleted=0 and Nama like '%" + txtSearch.getText() + "%'");
								break;
							default:
								rs = stm.executeQuery("SELECT NamaDivisi, Nama FROM Divisi " +
									"LEFT JOIN Karyawan ON Divisi.IDKepala=Karyawan.IDKaryawan and Karyawan.deleted=0 " +
									"WHERE Divisi.deleted=0");
								break;
						}
					}
					int i = 0;
					while (rs.next()) {
						for (int j = 0; j < col; j++) {
							data[i][j] = rs.getString(j + 1);
						}
						i++;
					}
					ShowData(colName, data);
				}
				if ("Employee's Position Data".equals(label)) {
					if (txtSearch.getText().equals("") || txtSearch.getText().equals("Enter Attribute. . .")) {
						rs = stm.executeQuery("SELECT COUNT(*) FROM Berpangkat,Pangkat,Karyawan " +
									"WHERE Pangkat.IDPangkat=Berpangkat.IDPangkat and Karyawan.IDKaryawan=Berpangkat.IDKaryawan " +
									"and Karyawan.deleted=0 and Pangkat.deleted=0");
					} else {
						switch (cbAttribute.getSelectedIndex()) {
							case 1:
								rs = stm.executeQuery("SELECT COUNT(*) FROM Berpangkat,Pangkat,Karyawan " +
									"WHERE Pangkat.IDPangkat=Berpangkat.IDPangkat and Karyawan.IDKaryawan=Berpangkat.IDKaryawan " +
									"and Karyawan.deleted=0 and Pangkat.deleted=0 and Nama like '%" + txtSearch.getText() + "%'");
								break;
							case 2:
								rs = stm.executeQuery("SELECT COUNT(*) FROM Berpangkat,Pangkat,Karyawan " +
									"WHERE Pangkat.IDPangkat=Berpangkat.IDPangkat and Karyawan.IDKaryawan=Berpangkat.IDKaryawan " +
									"and Karyawan.deleted=0 and Pangkat.deleted=0 and NamaKepangkatan like '%" + txtSearch.getText() + "%'");
								break;
							case 3:
								rs = stm.executeQuery("SELECT COUNT(*) FROM Berpangkat,Pangkat,Karyawan " +
									"WHERE Pangkat.IDPangkat=Berpangkat.IDPangkat and Karyawan.IDKaryawan=Berpangkat.IDKaryawan " +
									"and Karyawan.deleted=0 and Pangkat.deleted=0 and TglMenjabat like '%" + txtSearch.getText() + "%'");
								break;
							default:
								rs = stm.executeQuery("SELECT COUNT(*) FROM Berpangkat,Pangkat,Karyawan " +
									"WHERE Pangkat.IDPangkat=Berpangkat.IDPangkat and Karyawan.IDKaryawan=Berpangkat.IDKaryawan " +
									"and Karyawan.deleted=0 and Pangkat.deleted=0");
								break;
						}
					}
					rs.next();
					int row = rs.getInt(1);
					int col = 3;
					String[] colName = {"Nama Karyawan", "Pangkat", "Tanggal Menjabat"};
					String[][] data = new String[row][col];
					if (txtSearch.getText().equals("") || txtSearch.getText().equals("Enter Attribute. . .")) {
						rs = stm.executeQuery("SELECT Nama, NamaKepangkatan, TglMenjabat FROM Berpangkat,Pangkat,Karyawan " +
									"WHERE Pangkat.IDPangkat=Berpangkat.IDPangkat and Karyawan.IDKaryawan=Berpangkat.IDKaryawan " +
									"and Karyawan.deleted=0 and Pangkat.deleted=0");
					} else {
						switch (cbAttribute.getSelectedIndex()) {
							case 1:
								rs = stm.executeQuery("SELECT Nama, NamaKepangkatan, TglMenjabat FROM Berpangkat,Pangkat,Karyawan " +
									"WHERE Pangkat.IDPangkat=Berpangkat.IDPangkat and Karyawan.IDKaryawan=Berpangkat.IDKaryawan " +
									"and Karyawan.deleted=0 and Pangkat.deleted=0 and Nama like '%" + txtSearch.getText() + "%'");
								break;
							case 2:
								rs = stm.executeQuery("SELECT Nama, NamaKepangkatan, TglMenjabat FROM Berpangkat,Pangkat,Karyawan " +
									"WHERE Pangkat.IDPangkat=Berpangkat.IDPangkat and Karyawan.IDKaryawan=Berpangkat.IDKaryawan " +
									"and Karyawan.deleted=0 and Pangkat.deleted=0 and NamaKepangkatan like '%" + txtSearch.getText() + "%'");
								break;
							case 3:
								rs = stm.executeQuery("SELECT Nama, NamaKepangkatan, TglMenjabat FROM Berpangkat,Pangkat,Karyawan " +
									"WHERE Pangkat.IDPangkat=Berpangkat.IDPangkat and Karyawan.IDKaryawan=Berpangkat.IDKaryawan " +
									"and Karyawan.deleted=0 and Pangkat.deleted=0 and TglMenjabat like '%" + txtSearch.getText() + "%'");
								break;
							default:
								rs = stm.executeQuery("SELECT Nama, NamaKepangkatan, TglMenjabat FROM Berpangkat,Pangkat,Karyawan " +
									"WHERE Pangkat.IDPangkat=Berpangkat.IDPangkat and Karyawan.IDKaryawan=Berpangkat.IDKaryawan " +
									"and Karyawan.deleted=0 and Pangkat.deleted=0");
								break;
						}
					}
					int i = 0;
					while (rs.next()) {
						for (int j = 0; j < col; j++) {
							data[i][j] = rs.getString(j + 1);
						}
						i++;
					}
					ShowData(colName, data);
				}
			} catch (SQLException E) {
				E.printStackTrace();
			}
		}
    }//GEN-LAST:event_btnSearchActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
		// TODO add your handling code here:
		MainUI main = new MainUI();
		main.setVisible(true);
		main.setLabel("Display Data");
		dispose();
    }//GEN-LAST:event_jButton2ActionPerformed

    private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed
		// TODO add your handling code here:
		//untuk ambil value dari selected row
		//int i = Table.getSelectedRow();
		//Table.getValueAt(i,1).toString(); untuk mengambil value baris ke i kolom ke 1
		int n = Table.getSelectedRow();
		if (n == -1) {
			JOptionPane.showMessageDialog(this, "Please choose a record!", "Warning", JOptionPane.ERROR_MESSAGE, warningIcon);
		} else {
			try {

				if ("Division Data".equals(label)) {
					stm.executeUpdate("UPDATE Divisi SET deleted = 1 WHERE IDDivisi='" + Table.getValueAt(n, 0) + "'");
					rs = stm.executeQuery("SELECT COUNT(*) FROM Divisi WHERE deleted=0");
					rs.next();
					int row = rs.getInt(1);
					int col = 3;
					String[] colName = {"ID Divisi", "Nama Divisi", "Bidang"};
					String[][] data = new String[row][col];
					rs = stm.executeQuery("SELECT IDDivisi, NamaDivisi, Bidang FROM Divisi WHERE deleted=0");
					int i = 0;
					while (rs.next()) {
						for (int j = 0; j < col; j++) {
							data[i][j] = rs.getString(j + 1);
						}
						i++;
					}
					ShowData(colName, data);
				}
				if ("Employee Data".equals(label)) {
					stm.executeUpdate("UPDATE Karyawan SET deleted = 1 WHERE IDKaryawan='" + Table.getValueAt(n, 0) + "'");
					rs = stm.executeQuery("SELECT COUNT(*) FROM Karyawan " +
										"LEFT JOIN Divisi ON Divisi.IDDivisi = Karyawan.IDMenempati and Divisi.deleted=0 " +
										"LEFT JOIN Keahlian ON Keahlian.IDKeahlian = Karyawan.IDKeahlian and Keahlian.deleted=0 " +
										"WHERE Karyawan.deleted=0");
					rs.next();
					int row = rs.getInt(1);
					int col = 6;
					String[] colName = {"ID Karyawan", "Nama", "Alamat", "Tanggal Lahir", "Divisi", "Keahlian"};
					String[][] data = new String[row][col];
					rs = stm.executeQuery("SELECT IDKaryawan, Nama, Alamat, TglLahir, NamaDivisi, NamaKeahlian FROM Karyawan " +
										"LEFT JOIN Divisi ON Divisi.IDDivisi = Karyawan.IDMenempati and Divisi.deleted=0 " +
										"LEFT JOIN Keahlian ON Keahlian.IDKeahlian = Karyawan.IDKeahlian and Keahlian.deleted=0 " +
										"WHERE Karyawan.deleted=0");
					int i = 0;
					while (rs.next()) {
						for (int j = 0; j < col; j++) {
							data[i][j] = rs.getString(j + 1);
						}
						i++;
					}
					ShowData(colName, data);
				}
				if ("Expertise Data".equals(label)) {
					stm.executeUpdate("UPDATE Keahlian SET deleted = 1 WHERE IDKeahlian='" + Table.getValueAt(n, 0) + "'");
					rs = stm.executeQuery("SELECT COUNT(*) FROM Keahlian WHERE deleted=0");
					rs.next();
					int row = rs.getInt(1);
					int col = 3;
					String[] colName = {"ID Keahlian", "Nama Keahlian", "Keterangan"};
					String[][] data = new String[row][col];
					rs = stm.executeQuery("SELECT IDKeahlian, NamaKeahlian, Keterangan FROM Keahlian WHERE deleted=0");
					int i = 0;
					while (rs.next()) {
						for (int j = 0; j < col; j++) {
							data[i][j] = rs.getString(j + 1);
						}
						i++;
					}
					ShowData(colName, data);
				}
				if ("Position Data".equals(label)) {
					stm.executeUpdate("UPDATE Pangkat SET deleted = 1 WHERE IDPangkat='" + Table.getValueAt(n, 0) + "'");
					rs = stm.executeQuery("SELECT COUNT(*) FROM Pangkat WHERE deleted=0");
					rs.next();
					int row = rs.getInt(1);
					int col = 3;
					String[] colName = {"ID Pangkat", "Nama Kepangkatan", "Keterangan"};
					String[][] data = new String[row][col];
					rs = stm.executeQuery("SELECT IDPangkat, NamaKepangkatan, Keterangan FROM Pangkat WHERE deleted=0");
					int i = 0;
					while (rs.next()) {
						for (int j = 0; j < col; j++) {
							data[i][j] = rs.getString(j + 1);
						}
						i++;
					}
					ShowData(colName, data);
				}
				if ("Working Program Data".equals(label)) {
					stm.executeUpdate("UPDATE ProgramKerja SET deleted = 1 WHERE IDProKer='" + Table.getValueAt(n, 0) + "'");
					rs = stm.executeQuery("SELECT COUNT(*) FROM ProgramKerja WHERE deleted=0");
					rs.next();
					int row = rs.getInt(1);
					int col = 3;
					String[] colName = {"ID ProKer", "Nama ProKer", "Keterangan"};
					String[][] data = new String[row][col];
					rs = stm.executeQuery("SELECT IDProKer, NamaProker, Keterangan FROM ProgramKerja WHERE deleted=0");
					int i = 0;
					while (rs.next()) {
						for (int j = 0; j < col; j++) {
							data[i][j] = rs.getString(j + 1);
						}
						i++;
					}
					ShowData(colName, data);
				}
				/**
				 * if("Working Program Plan Data".equals(label)){ rs = stm.executeQuery("SELECT Count(*) FROM Rencana,ProgramKerja,Divisi " +"WHERE Rencana.IDProKer=ProgramKerja.IDProKer and Rencana.IDDivisi=Divisi.IDDivisi and ProgramKerja.deleted = 0 and Divisi.deleted = 0"); rs.next(); int row = rs.getInt(1); int col = 3; String[] colName = {"Program Kerja","Divisi","Tanggal Pelaksanaan"}; String[][] data = new String[row][col]; rs = stm.executeQuery("SELECT NamaProker,NamaDivisi,TglPelaksanaan FROM Rencana,ProgramKerja,Divisi " +"WHERE Rencana.IDProKer=ProgramKerja.IDProKer and Rencana.IDDivisi=Divisi.IDDivisi and ProgramKerja.deleted = 0 and Divisi.deleted = 0"); int i = 0; while(rs.next()){ for (int j = 0; j < col; j++) { data[i][j] = rs.getString(j+1); } i++; } ShowData(colName,data); }if("Supervisor Data".equals(label)){ stm.executeUpdate("UPDATE Karyawan A, Karyawan S SET IDSupervisor=NULL WHERE S.IDKaryawan=A.IDSupervisor and A.Nama='"+Table.getValueAt(n, 1)+"' and S.Nama='"+Table.getValueAt(n, 0)+"'"); rs = stm.executeQuery("SELECT COUNT(*) FROM Karyawan A,Karyawan S WHERE S.IDKaryawan = A.IDSupervisor and A.deleted = 0 and S.deleted = 0"); rs.next(); int row = rs.getInt(1); int col = 2; String[] colName = {"Nama Supervisor","Nama Karyawan"}; String[][] data = new String[row][col]; rs = stm.executeQuery("SELECT S.Nama,A.Nama FROM Karyawan A,Karyawan S " +"WHERE S.IDKaryawan = A.IDSupervisor and A.deleted = 0 and S.deleted = 0"); int i = 0; while(rs.next()){ for (int j = 0; j < col; j++) { data[i][j] = rs.getString(j+1); } i++; } ShowData(colName,data); }if("Division Superintendent Data".equals(label)){ stm.executeUpdate("UPDATE Divisi,Karyawan SET IDMengawas=NULL WHERE IDDivisi=IDMengawas and Nama='"+Table.getValueAt(n, 1)+"' and NamaDivisi='"+Table.getValueAt(n, 0)+"'"); rs = stm.executeQuery("SELECT COUNT(*) FROM Divisi,Karyawan WHERE IDDivisi=IDMengawas and Divisi.deleted = 0 and Karyawan.deleted = 0"); rs.next(); int row = rs.getInt(1); int col = 2; String[] colName = {"Divisi","Pengawas Divisi"}; String[][] data = new String[row][col]; rs = stm.executeQuery("SELECT NamaDivisi,Nama FROM Divisi,Karyawan WHERE IDDivisi=IDMengawas and Divisi.deleted = 0 and Karyawan.deleted = 0"); int i = 0; while(rs.next()){ for (int j = 0; j < col; j++) { data[i][j] = rs.getString(j+1); } i++; } ShowData(colName,data); }if("Head of Division Data".equals(label)){ stm.executeUpdate("UPDATE Divisi,Karyawan SET IDKepala=NULL WHERE IDKepala=IDKaryawan and Nama='"+Table.getValueAt(n, 1)+"' and NamaDivisi='"+Table.getValueAt(n, 0)+"'"); rs = stm.executeQuery("SELECT COUNT(*) FROM Divisi,Karyawan WHERE IDKepala=IDKaryawan and Divisi.deleted = 0 and Karyawan.deleted = 0"); rs.next(); int row = rs.getInt(1); int col = 2; String[] colName = {"Divisi","Kepala Divisi"}; String[][] data = new String[row][col]; rs = stm.executeQuery("SELECT NamaDivisi,Nama FROM Divisi,Karyawan WHERE IDKepala=IDKaryawan and Divisi.deleted = 0 and Karyawan.deleted = 0"); int i = 0; while(rs.next()){ for (int j = 0; j < col; j++) { data[i][j] = rs.getString(j+1); } i++; } ShowData(colName,data); }if("Employee's Position Data".equals(label)){ rs = stm.executeQuery("SELECT COUNT(*) FROM Berpangkat,Pangkat,Karyawan WHERE Berpangkat.IDPangkat=Pangkat.IDPangkat and Berpangkat.IDKaryawan=Karyawan.IDKaryawan and Pangkat.deleted = 0 and Karyawan.deleted = 0"); rs.next(); int row = rs.getInt(1); int col = 3; String[] colName = {"Nama Karyawan","Pangkat","Tanggal Menjabat"}; String[][] data = new String[row][col]; rs = stm.executeQuery("SELECT Nama,NamaKepangkatan,TglMenjabat FROM Berpangkat,Pangkat,Karyawan " +"WHERE Berpangkat.IDPangkat=Pangkat.IDPangkat and Berpangkat.IDKaryawan=Karyawan.IDKaryawan and Pangkat.deleted = 0 and Karyawan.deleted = 0"); int i = 0; while(rs.next()){ for (int j = 0; j < col; j++) { data[i][j] = rs.getString(j+1); } i++; } ShowData(colName,data); }
				 */
			} catch (SQLException E) {
				E.printStackTrace();
			}
		}
    }//GEN-LAST:event_btnDeleteActionPerformed

	public void ShowData(String[] cols, String[][] data) {
		DefaultTableModel model = (DefaultTableModel) Table.getModel();
		model.setDataVector(data, cols);
	}

	public void ClearData(String[] cols) {
		Table.setModel(new DefaultTableModel(null, cols));
	}

	/**
	 * @param args the command line arguments
	 */
	public static void main(String args[]) {
		/* Set the Nimbus look and feel */
		//<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
		/* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
		 */
		try {
			for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
				if ("Nimbus".equals(info.getName())) {
					javax.swing.UIManager.setLookAndFeel(info.getClassName());
					break;
				}
			}
		} catch (ClassNotFoundException ex) {
			java.util.logging.Logger.getLogger(DataUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		} catch (InstantiationException ex) {
			java.util.logging.Logger.getLogger(DataUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		} catch (IllegalAccessException ex) {
			java.util.logging.Logger.getLogger(DataUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		} catch (javax.swing.UnsupportedLookAndFeelException ex) {
			java.util.logging.Logger.getLogger(DataUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		}
		//</editor-fold>

		/* Create and display the form */
		java.awt.EventQueue.invokeLater(new Runnable() {
			public void run() {
				new DataUI().setVisible(true);
			}
		});
	}

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JTable Table;
    private javax.swing.JButton btnDelete;
    private javax.swing.JButton btnSearch;
    private javax.swing.JButton btnUpdate;
    private javax.swing.JComboBox<String> cbAttribute;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel lblJudul;
    private com.mycompany.projectsbd.MyTextField txtSearch;
    // End of variables declaration//GEN-END:variables
}
