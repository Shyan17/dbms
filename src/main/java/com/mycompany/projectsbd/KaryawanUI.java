/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JFrame.java to edit this template
 */
package com.mycompany.projectsbd;

import com.github.lgooddatepicker.components.DatePickerSettings;
import java.awt.Color;
import java.awt.Image;
import java.sql.*;
import javax.swing.Icon;
import javax.swing.JOptionPane;
import javax.swing.ImageIcon;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Lenovo
 */
public class KaryawanUI extends javax.swing.JFrame {

    /**
     * Creates new form Karyawan
     */
	int tabRow;
	javax.swing.JTable Table;
    Connection conn;
	Statement stm;
	ResultSet rs;
	ImageIcon rawErrorImage = new ImageIcon("src/main/resources/Error.png");
	ImageIcon rawSuccessImage = new ImageIcon("src/main/resources/Success.png");
	Image errorImage = rawErrorImage.getImage();
	Image successImage = rawSuccessImage.getImage();
	Image errorScaledImage = errorImage.getScaledInstance(50, 50, Image.SCALE_DEFAULT);
	Image successScaledImage = successImage.getScaledInstance(50, 50, Image.SCALE_DEFAULT);
	Icon errorIcon = new ImageIcon( errorScaledImage );
	Icon successIcon = new ImageIcon( successScaledImage );
        //warning icon
        ImageIcon rawWarningImage = new ImageIcon("src/main/resources/Warning.png");
        Image warningImage = rawWarningImage.getImage();
        Image warningScaledImage = warningImage.getScaledInstance(50, 50, Image.SCALE_DEFAULT);
        Icon warningIcon = new ImageIcon( warningScaledImage );
        
        
    public KaryawanUI() {
        initComponents();
		DatePickerSettings dateSettings = new DatePickerSettings();
		dateSettings.setFormatForDatesCommonEra("yyyy-MM-dd");
		dateSettings.setFormatForDatesBeforeCommonEra("uuuu-MM-dd");
		dpTglLahir.setSettings(dateSettings);
        String hostname = "localhost";
        String sqlInstanceName = "LAPTOP-M6P0LRI6"; //computer name 
        String sqlDatabase = "dbPerusahaan2022";  //sql server database name
        String sqlUser = "sa";//username akun
        String sqlPassword = "ggilkom"; //passwrod sa account
        String driver = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
        String connectURL = "jdbc:sqlserver://" + hostname + ":1433" 
                        + ";instance=" + sqlInstanceName + ";databaseName=" + sqlDatabase+";encrypt=true;trustServerCertificate=true";
        try{
            Class.forName(driver);
            conn = DriverManager.getConnection(connectURL, sqlUser, sqlPassword);
            System.out.println("Connect to database successful!!"); 
			stm = conn.createStatement();
			rs = stm.executeQuery("SELECT IDDivisi,NamaDivisi FROM Divisi where deleted=0");
			while(rs.next()){
				cbDivisi.addItem(rs.getString("IDDivisi")+"|"+rs.getString("NamaDivisi"));
			}
			rs = stm.executeQuery("SELECT IDKeahlian,NamaKeahlian FROM Keahlian where deleted=0");
			while(rs.next()){
				cbKeahlian.addItem(rs.getString("IDKeahlian")+"|"+rs.getString("NamaKeahlian"));
			}
        }catch(ClassNotFoundException E){
            E.printStackTrace();
        }catch(SQLException E){
            E.printStackTrace();
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        dpTglLahir = new com.github.lgooddatepicker.components.DatePicker();
        btnInsert = new javax.swing.JButton();
        txtAlamat = new javax.swing.JTextField();
        btnClear = new javax.swing.JButton();
        cbDivisi = new javax.swing.JComboBox<>();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        txtNamaKaryawan = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        btnBack = new javax.swing.JButton();
        cbKeahlian = new javax.swing.JComboBox<>();
        jPanel2 = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Employee");
        setPreferredSize(new java.awt.Dimension(635, 470));
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel1.setBackground(new java.awt.Color(57, 67, 76));
        jPanel1.setForeground(new java.awt.Color(153, 153, 255));

        dpTglLahir.setBackground(new java.awt.Color(154, 159, 152));
        dpTglLahir.setForeground(new java.awt.Color(14, 94, 111));

        btnInsert.setText("Insert");
        btnInsert.setBackground(new java.awt.Color(255, 239, 214));
        btnInsert.setFont(new java.awt.Font("Segoe UI", 1, 16)); // NOI18N
        btnInsert.setForeground(new java.awt.Color(14, 94, 111));
        btnInsert.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnInsertActionPerformed(evt);
            }
        });

        txtAlamat.setBackground(new java.awt.Color(255, 255, 255));
        txtAlamat.setForeground(new java.awt.Color(0, 0, 0));
        txtAlamat.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtAlamatActionPerformed(evt);
            }
        });

        btnClear.setText("Clear");
        btnClear.setBackground(new java.awt.Color(255, 239, 214));
        btnClear.setFont(new java.awt.Font("Segoe UI", 1, 16)); // NOI18N
        btnClear.setForeground(new java.awt.Color(14, 94, 111));
        btnClear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnClearActionPerformed(evt);
            }
        });

        cbDivisi.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Choose Division" }));
        cbDivisi.setBackground(new java.awt.Color(255, 255, 255));
        cbDivisi.setFont(new java.awt.Font("Segoe UI", 0, 15)); // NOI18N
        cbDivisi.setForeground(new java.awt.Color(0, 0, 0));
        cbDivisi.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbDivisiActionPerformed(evt);
            }
        });

        jLabel4.setText("Date of Birth");
        jLabel4.setFont(new java.awt.Font("SimSun", 1, 18)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(255, 244, 152));

        jLabel5.setText("Division");
        jLabel5.setFont(new java.awt.Font("SimSun", 1, 18)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(255, 244, 152));

        jLabel3.setText("Address");
        jLabel3.setFont(new java.awt.Font("SimSun", 1, 18)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 244, 152));

        txtNamaKaryawan.setBackground(new java.awt.Color(255, 255, 255));
        txtNamaKaryawan.setForeground(new java.awt.Color(0, 0, 0));
        txtNamaKaryawan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtNamaKaryawanActionPerformed(evt);
            }
        });

        jLabel6.setText("Expertise");
        jLabel6.setFont(new java.awt.Font("SimSun", 1, 18)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(255, 244, 152));

        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("EMPLOYEE DATA INPUT");
        jLabel1.setFont(new java.awt.Font("Barlow", 1, 30)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));

        jLabel2.setText("Name");
        jLabel2.setFont(new java.awt.Font("SimSun", 1, 18)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 244, 152));

        btnBack.setText("Back");
        btnBack.setBackground(new java.awt.Color(255, 239, 214));
        btnBack.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        btnBack.setForeground(new java.awt.Color(14, 94, 111));
        btnBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBackActionPerformed(evt);
            }
        });

        cbKeahlian.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Choose Expertise" }));
        cbKeahlian.setBackground(new java.awt.Color(255, 255, 255));
        cbKeahlian.setFont(new java.awt.Font("Segoe UI", 0, 15)); // NOI18N
        cbKeahlian.setForeground(new java.awt.Color(0, 0, 0));

        jPanel2.setBackground(new java.awt.Color(107, 196, 196));

        jLabel7.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel7.setText("Let's Fill !");
        jLabel7.setFont(new java.awt.Font("Futura XBlkIt BT", 1, 24)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(50, 43, 27));

        jLabel8.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel8.setText("Please fill all of");
        jLabel8.setFont(new java.awt.Font("Futura Md BT", 0, 18)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(50, 43, 27));

        jLabel9.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel9.setText("the section on the form");
        jLabel9.setFont(new java.awt.Font("Futura Md BT", 0, 18)); // NOI18N
        jLabel9.setForeground(new java.awt.Color(50, 43, 27));

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jLabel9, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 206, Short.MAX_VALUE)
                    .addComponent(jLabel8, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel7, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(0, 21, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(151, 151, 151)
                .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel8)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel9)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(27, 27, 27)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLabel3)
                                            .addComponent(jLabel2))
                                        .addGap(68, 68, 68)
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                            .addComponent(txtAlamat, javax.swing.GroupLayout.DEFAULT_SIZE, 210, Short.MAX_VALUE)
                                            .addComponent(txtNamaKaryawan)))
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addGap(35, 35, 35)
                                        .addComponent(btnInsert, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(30, 30, 30)
                                        .addComponent(btnClear, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addGap(10, 10, 10)
                                        .addComponent(jLabel1)))
                                .addGap(0, 2, Short.MAX_VALUE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel4)
                                    .addComponent(jLabel5)
                                    .addComponent(jLabel6))
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(dpTglLahir, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addGap(13, 13, 13)
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(cbKeahlian, javax.swing.GroupLayout.Alignment.TRAILING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addComponent(cbDivisi, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))))))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(16, 16, 16)
                        .addComponent(btnBack)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addGap(18, 18, 18)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(jLabel1)
                .addGap(27, 27, 27)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtNamaKaryawan, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addGap(22, 22, 22)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtAlamat, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3))
                .addGap(27, 27, 27)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(dpTglLahir, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4))
                .addGap(22, 22, 22)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(cbDivisi, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(cbKeahlian, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 27, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnInsert, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnClear, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnBack, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(13, 13, 13))
            .addComponent(jPanel2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        getContentPane().add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, -1));

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents
	public void passTable(javax.swing.JTable Table,int i){
		this.Table = Table;
		this.tabRow = i;
	}
    private void btnInsertActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnInsertActionPerformed
        // TODO add your handling code here:
        if (txtNamaKaryawan.getText().equals("") || txtAlamat.getText().equals("") || dpTglLahir.getText().equals("") || cbKeahlian.getSelectedIndex() == 0 || cbDivisi.getSelectedIndex() == 0) {
			JOptionPane.showMessageDialog(this, "Please fill the blank sections!", "Warning", JOptionPane.ERROR_MESSAGE, warningIcon);
		} else {
			try {
				if (btnInsert.getText().equals("Insert")) {
					rs = stm.executeQuery("SELECT * FROM Karyawan WHERE Nama = '" + txtNamaKaryawan.getText() + "' and Alamat = '" + txtAlamat.getText() + "' and TglLahir = '" + dpTglLahir.getText() + "'");
					if (rs.next()) {
						JOptionPane.showMessageDialog(this, "Record already exists!", "Error", JOptionPane.ERROR_MESSAGE, errorIcon);
					} else {
						rs = stm.executeQuery("SELECT COUNT(*) FROM Karyawan");
						rs.next();
						int IDNum = rs.getInt(1) + 1;
						String ID = String.format("K%05d", IDNum);
						stm.executeUpdate("INSERT INTO Karyawan (IDKaryawan,Nama,Alamat,TglLahir,IDMenempati,IDKeahlian) values('" + ID + "','" + txtNamaKaryawan.getText() + "','" + txtAlamat.getText() + "','" + dpTglLahir.getText() + "','"
								+ cbDivisi.getSelectedItem().toString().substring(0, 6) + "','" + cbKeahlian.getSelectedItem().toString().substring(0, 6) + "')");
						JOptionPane.showMessageDialog(this, "Insert successful!", "Success", JOptionPane.INFORMATION_MESSAGE, successIcon);
					}
				} else {
					stm.executeUpdate("UPDATE Karyawan SET Nama='" + txtNamaKaryawan.getText() + "',Alamat='" + txtAlamat.getText() + "',TglLahir='"+dpTglLahir.getText()+"',IDMenempati='"
							+cbDivisi.getSelectedItem().toString().substring(0,6)+"',IDKeahlian='"+cbKeahlian.getSelectedItem().toString().substring(0,6)+"' WHERE IDKaryawan='" + Table.getValueAt(tabRow, 0).toString() + "'");
					rs = stm.executeQuery("SELECT COUNT(*) FROM Karyawan " +
									"LEFT JOIN Divisi ON Divisi.IDDivisi = Karyawan.IDMenempati and Divisi.deleted=0 " +
									"LEFT JOIN Keahlian ON Keahlian.IDKeahlian = Karyawan.IDKeahlian and Keahlian.deleted=0 " +
									"WHERE Karyawan.deleted=0");
					rs.next();
					int row = rs.getInt(1);
					int col = 6;
					String[] colName = {"ID Karyawan", "Nama", "Alamat", "Tanggal Lahir", "Divisi", "Keahlian"};
					String[][] data = new String[row][col];
					rs = stm.executeQuery("SELECT IDKaryawan, Nama, Alamat, TglLahir, NamaDivisi, NamaKeahlian FROM Karyawan " +
										"LEFT JOIN Divisi ON Divisi.IDDivisi = Karyawan.IDMenempati and Divisi.deleted=0 " +
										"LEFT JOIN Keahlian ON Keahlian.IDKeahlian = Karyawan.IDKeahlian and Keahlian.deleted=0 " +
										"WHERE Karyawan.deleted=0");
					int i = 0;
					while (rs.next()) {
						for (int j = 0; j < col; j++) {
							data[i][j] = rs.getString(j + 1);
						}
						i++;
					}
					DefaultTableModel model = (DefaultTableModel) Table.getModel();
					model.setDataVector(data, colName);
					dispose();
				}
        }catch(SQLException E){
            E.printStackTrace();
        }
        }
        
    }//GEN-LAST:event_btnInsertActionPerformed

    private void btnBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBackActionPerformed
        // TODO add your handling code here:
        if(btnInsert.getText().equals("Insert")){
			MainUI main = new MainUI();
			main.setVisible(true);
			main.setLabel("Choose Data to Input");
			dispose();
		}else{
			dispose();
		}
    }//GEN-LAST:event_btnBackActionPerformed

    private void btnClearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnClearActionPerformed
        // TODO add your handling code here:
		txtNamaKaryawan.setText("");
		txtAlamat.setText("");
		dpTglLahir.setText("");
		cbDivisi.setSelectedIndex(0);
		cbKeahlian.setSelectedIndex(0);
    }//GEN-LAST:event_btnClearActionPerformed

    private void txtAlamatActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtAlamatActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtAlamatActionPerformed

    private void cbDivisiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbDivisiActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cbDivisiActionPerformed

    private void txtNamaKaryawanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtNamaKaryawanActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtNamaKaryawanActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(KaryawanUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(KaryawanUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(KaryawanUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(KaryawanUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new KaryawanUI().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBack;
    private javax.swing.JButton btnClear;
    public javax.swing.JButton btnInsert;
    public javax.swing.JComboBox<String> cbDivisi;
    public javax.swing.JComboBox<String> cbKeahlian;
    public com.github.lgooddatepicker.components.DatePicker dpTglLahir;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    public javax.swing.JTextField txtAlamat;
    public javax.swing.JTextField txtNamaKaryawan;
    // End of variables declaration//GEN-END:variables
}
