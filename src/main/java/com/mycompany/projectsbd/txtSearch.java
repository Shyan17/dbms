/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.projectsbd;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JTextField;

/**
 *
 * @author harit
 */
public class txtSearch extends JTextField{

	/**
	 * @return the prefixIconm
	 */
	public Icon getPrefixIconm() {
		return prefixIconm;
	}

	/**
	 * @param prefixIconm the prefixIconm to set
	 */
	public void setPrefixIconm(Icon prefixIcon) {
		this.prefixIconm = prefixIconm;
		initBorder();
	}

	/**
	 * @return the suffixIcon
	 */
	public Icon getSuffixIcon() {
		return suffixIcon;
	}

	/**
	 * @param suffixIcon the suffixIcon to set
	 */
	public void setSuffixIcon(Icon suffixIcon) {
		this.suffixIcon = suffixIcon;
		initBorder();
	}
	private Icon prefixIconm;
	private Icon suffixIcon;
	public txtSearch(){
		setBorder(javax.swing.BorderFactory.createEmptyBorder(5, 5, 5, 5));
	}


	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g); 
		paintIcon(g);
		//  paint border
        if (isFocusOwner()) {
            g.setColor(new Color(4, 88, 167));
            g.drawRect(0, 0, getWidth() - 1, getHeight() - 1);
            g.drawRect(1, 1, getWidth() - 3, getHeight() - 3);
        } else {
            g.setColor(new Color(142, 142, 142));
            g.drawRect(0, 0, getWidth() - 1, getHeight() - 1);
            g.drawRect(1, 1, getWidth() - 3, getHeight() - 3);
        }
	}
	private void paintIcon(Graphics g){
		Graphics2D g2 = (Graphics2D) g;
		if (prefixIconm != null) {
            Image prefix = ((ImageIcon) prefixIconm).getImage();
            int y = (getHeight() - prefixIconm.getIconHeight()) / 2;
            g2.drawImage(prefix, 3, y, this);
        }
	}
			
	private void initBorder(){
		int left=5;
		int right=5;
		//5 is default
		if(prefixIconm!=null){
			left = prefixIconm.getIconWidth()+5;
		}
		if(suffixIcon!=null){
			right = suffixIcon.getIconWidth();
		}
		setBorder(javax.swing.BorderFactory.createEmptyBorder(5, 5, 5, 5));
		
	}
}
